package in.dynamicvishva.care4meuser;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import in.dynamicvishva.care4meuser.CloudMessaging.CloudMessagingHelper;
import in.dynamicvishva.care4meuser.Dashboard.DashboardFragment;
import in.dynamicvishva.care4meuser.Dashboard.Model.UserDetailsModel;
import in.dynamicvishva.care4meuser.Login.LoginActivity;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.Profile.ProfileFragment;
import in.dynamicvishva.care4meuser.Review.ReviewFragment;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Context mCon;
    private String mobileNo, checkFetched, userId, seeCheck;
    private ProgressDialog progressDialog;
    private UserDetailsModel userDetailsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCon = this;
        progressDialog = new ProgressDialog(mCon);

        if(PreferenceUtil.isUserLoggedIn())
        {
            if (PreferenceUtil.getUser() != null) {

                BottomNavigationView bottomNavigationView = findViewById(R.id.botnav);
                bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new DashboardFragment()).commit();
            } else {
                mobileNo = getIntent().getStringExtra("mobileNo");
                fetchUserDetails(mobileNo);
            }
        }
        else if (!PreferenceUtil.isUserLoggedIn())
        {
            startActivity(new Intent(mCon, LoginActivity.class));
            finish();
        }

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selFrag = null;

                    switch (menuItem.getItemId()) {
                        case R.id.nav_home:
                            selFrag = new DashboardFragment();
                            break;

                        case R.id.nav_reviews:
                            selFrag = new ReviewFragment();
                            break;

                        case R.id.nav_profile:
                            selFrag = new ProfileFragment();
                            break;

                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.container, selFrag).commit();

                    return true;
                }
            };

    private Boolean exit = false;

    private void fetchUserDetails(String mobileNo) {
        progressDialog.setMessage("Fetching Details");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<UserDetailsModel> call = ApiClient.getNetworkService().fetchUserDetails(mobileNo);
        call.enqueue(new Callback<UserDetailsModel>() {
            @Override
            public void onResponse(Call<UserDetailsModel> call, Response<UserDetailsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.d("checkthis", "inside method");
                        userDetailsModel = response.body();
                        PreferenceUtil.clearUserData();
                        PreferenceUtil.setUser(userDetailsModel);
                        BottomNavigationView bottomNavigationView = findViewById(R.id.botnav);
                        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new DashboardFragment()).commit();
                        progressDialog.dismiss();
                    }
                } else if (response.code() == 401) {
                    PreferenceUtil.clearAll();
                    mCon.startActivity(new Intent(mCon, LoginActivity.class));
                    ((Activity) mCon).finish();
                    progressDialog.dismiss();
                } else {
                    Log.d("check", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<UserDetailsModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("check", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(mCon, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }

    }
}