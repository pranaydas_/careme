package in.dynamicvishva.care4meuser.Network;

import android.app.Notification;

import java.util.List;

import in.dynamicvishva.care4meuser.CloudMessaging.Model.NotificationModel;
import in.dynamicvishva.care4meuser.Dashboard.Model.DashboardTasks;
import in.dynamicvishva.care4meuser.Dashboard.Model.TaskStatusModel;
import in.dynamicvishva.care4meuser.Dashboard.Model.UserDetailsModel;
import in.dynamicvishva.care4meuser.Profile.Model.Questions;
import in.dynamicvishva.care4meuser.Profile.Model.UserExperience;
import in.dynamicvishva.care4meuser.Profile.Model.UserUpdateModel;
import in.dynamicvishva.care4meuser.Review.Model.AddReviewModel;
import in.dynamicvishva.care4meuser.Review.Model.ReviewModel;
import in.dynamicvishva.care4meuser.Signup.Model.CityList;
import in.dynamicvishva.care4meuser.Signup.Model.OnlyService;
import in.dynamicvishva.care4meuser.Signup.Model.SignUpModel;
import in.dynamicvishva.care4meuser.Signup.Model.StateList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface NetworkService {
    @POST("state.php")
    Call<List<StateList>> fetchStates();

    @POST("city.php")
    Call<List<CityList>> fetchCities(@Header("Stateid") int stateId);

    @POST("user_registration.php")
    Call<String> signUpUser(@Body SignUpModel signUpModel);

    @POST("login.php")
    Call<String> loginUser(@Header("Mobileno") String mobileNo, @Header("Pass") String password);

    @POST("user_details.php")
    Call<UserDetailsModel> fetchUserDetails(@Header("Mobileno") String mobileNo);

    @POST("update_user.php")
    Call<String> updateUser(@Body UserUpdateModel userUpdateModel);

    @POST("user_day_wise_rating.php")
    Call<List<ReviewModel>> fetchReviews(@Header("Userid") String userId);

    @POST("user_dashboard.php")
    Call<DashboardTasks> fetchDashboardTasks(@Header("Userid") String userId, @Header("Date") String date);

    @POST("user_add_task_status.php")
    Call<String> addTask(@Body TaskStatusModel taskStatusModel);

    @POST("user_update_task_status.php")
    Call<String> updateTask(@Body TaskStatusModel taskStatusModel);

    @POST("user_popup.php")
    Call<String> checkPopup(@Header("Customerid") String customerId, @Header("Userid") String userId);

    @POST("customer_add_day_wise_review.php")
    Call<String> addReview(@Body AddReviewModel addReviewModel);

    @POST("question_api.php")
    Call<Questions> getQuestions(@Header("Mobileno") String mobileNo);

    @POST("generate_otp.php")
    Call<String> getOtp(@Header("Mobileno") String mobileNo);

    @POST("add_otp.php")
    Call<String> sendOtp(@Header("Mobileno") String mobileNo, @Header("Otp") String otp);

    @POST("forgot_password.php")
    Call<String> resetPassword(@Header("Mobileno") String mobileNo, @Header("Pass") String password);

    @POST("fetch_service.php")
    Call<List<OnlyService>> fetchServices();

    @POST("user_experience.php")
    Call<List<UserExperience>> fetchExperience(@Header("Userid") String userId);

    @POST("notification.php")
    Call<String> sendCloudMessagingToken(@Body NotificationModel notificationModel);
}