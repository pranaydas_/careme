package in.dynamicvishva.care4meuser.Base;

import android.app.Application;
import android.content.Context;

import in.dynamicvishva.care4meuser.BuildConfig;
import com.facebook.stetho.Stetho;

public class App extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(App.this);
        }
    }

    public static Context getContext() {
        return context;
    }
}

