package in.dynamicvishva.care4meuser.Signup.Model;

import com.google.gson.annotations.SerializedName;

public class OnlyService {

    @SerializedName("srId")
    private String srId;

    @SerializedName("serviceId")
    private String serviceId;

    @SerializedName("serviceName")
    private String serviceName;

    @SerializedName("amount")
    private String amount;

    @SerializedName("status")
    private String status;

    @SerializedName("createdBy")
    private String createdBy;

    @SerializedName("createdDate")
    private String createdDate;

    public OnlyService(){}

    public OnlyService(String srId, String serviceId, String serviceName, String amount, String status, String createdBy, String createdDate) {
        this.srId = srId;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.amount = amount;
        this.status = status;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public String getSrId() {
        return srId;
    }

    public void setSrId(String srId) {
        this.srId = srId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "OnlyService{" +
                "srId='" + srId + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", amount='" + amount + '\'' +
                ", status='" + status + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
