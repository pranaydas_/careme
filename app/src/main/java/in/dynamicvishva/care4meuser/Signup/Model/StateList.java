package in.dynamicvishva.care4meuser.Signup.Model;

import com.google.gson.annotations.SerializedName;

public class StateList
{
    @SerializedName("id")
    private int stateId;

    @SerializedName("name")
    private String stateName;

    public StateList() {}

    public StateList(int stateId, String stateName) {
        this.stateId = stateId;
        this.stateName = stateName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        return "StateList{" +
                "stateId='" + stateId + '\'' +
                ", stateName='" + stateName + '\'' +
                '}';
    }
}
