package in.dynamicvishva.care4meuser.Signup;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.dynamicvishva.care4meuser.Dashboard.Adapter.DashboardTasksAdapter;
import in.dynamicvishva.care4meuser.Login.LoginActivity;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Signup.Model.CityList;
import in.dynamicvishva.care4meuser.Signup.Model.OnlyService;
import in.dynamicvishva.care4meuser.Signup.Model.SignUpModel;
import in.dynamicvishva.care4meuser.Signup.Model.StateList;
import in.dynamicvishva.care4meuser.Util.ResponseCodes;
import in.dynamicvishva.care4meuser.databinding.ActivitySignUpBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    private ActivitySignUpBinding binding;
    private Context mCon;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String nameStr, emailStr, mobileNoStr, addressStr, pincodeStr,
            cityStr, stateStr, tempAddressStr, allServiceId, allServiceMonth="", allServiceYear="";
    private String passwordStr, confPassStr, genderStr, expStr = "No", ageStr, aadharNoStr, panNoStr, qualifyStr;
    private int statePos, cityPos, selId;
    private List<String> states, cities, ansYear, ansMonth;
    private List<Integer> statesIdDb;
    ArrayAdapter stateAdapter, cityAdapter;
    private String aadharEncodedBase64, aadharFileName, panEncodedBase64, panFileName;
    private int REQUEST_CAMERA = 1, SELECT_FILE_AADHAR = 10, SELECT_FILE_PAN = 11;
    private static int MY_PERMISSIONS_REQUEST_CAMERA = 11;
    private static int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 12;
    private List<StateList> stateList = new ArrayList<>();
    private List<CityList> cityList = new ArrayList<>();
    private boolean isValidExperience = false;
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        mCon = this;

        loadState();

        binding.aadharPicSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mCon,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

                } else if (ContextCompat.checkSelfPermission(mCon,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

                } else {
                    aadharIntent();
                }
            }
        });

        binding.panPicSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mCon,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

                } else if (ContextCompat.checkSelfPermission(mCon,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

                } else {
                    panIntent();
                }
            }
        });

        //Experience
        if (binding.experienceRadioGroup.getCheckedRadioButtonId() == -1) {
            binding.experienceErrorTextView.setVisibility(View.VISIBLE);
            isValidExperience = false;
        } else {
            isValidExperience = true;
            binding.experienceRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (binding.yesRadioButton.isChecked()) {
                        expStr = "Yes";
                        binding.experienceErrorTextView.setVisibility(View.GONE);
                        binding.layout1.setVisibility(View.VISIBLE);
                        binding.layout2.setVisibility(View.VISIBLE);
                    } else if (binding.noRadioButton.isChecked()) {
                        expStr = "No";
                        binding.experienceErrorTextView.setVisibility(View.GONE);
                        binding.layout1.setVisibility(View.GONE);
                        binding.layout2.setVisibility(View.GONE);
                    }
                }
            });
        }

        binding.btnSignUp.setOnClickListener(v -> {
            nameStr = binding.nameEditText.getText().toString().trim();
            emailStr = binding.emailEditText.getText().toString().trim();
            mobileNoStr = binding.mobileNoEditText.getText().toString().trim();
            addressStr = binding.addressEditText.getText().toString().trim();
            tempAddressStr = binding.tempAddressEditText.getText().toString().trim();
            pincodeStr = binding.pincodeEditText.getText().toString().trim();
            ageStr = binding.ageEditText.getText().toString();
            aadharNoStr = binding.aadharEditText.getText().toString();
            panNoStr = binding.panEditText.getText().toString();
            passwordStr = binding.passwordEditText.getText().toString().trim();
            confPassStr = binding.confirmPasswordEditText.getText().toString().trim();

            validate();
        });
    }

    private void loadState() {
        try {
            Call<List<StateList>> call = ApiClient.getNetworkService().fetchStates();

            call.enqueue(new Callback<List<StateList>>() {
                @Override
                public void onResponse(Call<List<StateList>> call, Response<List<StateList>> response) {
                    if (response.isSuccessful()) {
                        stateList = response.body();

                        if (stateList != null) {

                            states = new ArrayList<>();
                            statesIdDb = new ArrayList<>();
                            states.add("Select State");
                            statesIdDb.add(0);

                            for (StateList model : stateList) {
                                states.add(model.getStateName());
                                statesIdDb.add(model.getStateId());
                            }

                            stateAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, states);
                            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.stateSpinner.setAdapter(stateAdapter);

                            binding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    stateStr = binding.stateSpinner.getSelectedItem().toString().trim();
                                    statePos = binding.stateSpinner.getSelectedItemPosition();

                                    selId = statesIdDb.get(statePos);

                                    loadCities(selId);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<StateList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCities(int stateId) {
        try {
            Call<List<CityList>> call = ApiClient.getNetworkService().fetchCities(stateId);

            call.enqueue(new Callback<List<CityList>>() {
                @Override
                public void onResponse(Call<List<CityList>> call, Response<List<CityList>> response) {
                    if (response.isSuccessful()) {
                        cityList = response.body();

                        if (cityList != null) {

                            cities = new ArrayList<>();
                            cities.add("Select City");

                            for (CityList model : cityList) {
                                cities.add(model.getCityName());
                            }

                            cityAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, cities);
                            cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.citySpinner.setAdapter(cityAdapter);

                            binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    cityStr = binding.citySpinner.getSelectedItem().toString();
                                    cityPos = binding.citySpinner.getSelectedItemPosition();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    }
                }

                @Override
                public void onFailure(Call<List<CityList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void aadharIntent() {
        String[] mimeTypes = {"image/jpeg", "image/jpg"};
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), SELECT_FILE_AADHAR);
    }

    private void panIntent() {
        String[] mimeTypes = {"image/jpeg", "image/jpg"};
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), SELECT_FILE_PAN);
    }

    private void validate() {
        boolean isValidName = false, isValidMobileNo = false, isValidMailId = false, isValidAddress = false, isValidPincode = false,
                isValidPass = false, isValidConfPass = false, isValidGender = false, isValidAge = false, isValidCity = false,
                isValidAadharNo = false, isValidPanNo = false, isValidQualification = false, isValidCourseMonth = false,
                isValidCourseYear = false;

        //Name
        if (TextUtils.isEmpty(nameStr)) {
            binding.nameTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.nameTextLayout.setError(null);
            isValidName = true;
        }

        //Mobile Number
        if (mobileNoStr.length() > 0) {
            if (mobileNoStr.length() < 10) {
                binding.mobileNoTextLayout.setError(getResources().getString(R.string.enter_valid_no));
            } else if (!mobileNoStr.startsWith("6") && !mobileNoStr.startsWith("7") && !mobileNoStr.startsWith("8") && !mobileNoStr.startsWith("9")) {
                binding.mobileNoTextLayout.setError("" + getResources().getString(R.string.enter_valid_no));
            } else {
                isValidMobileNo = true;
                binding.mobileNoTextLayout.setError(null);
            }
        } else {
            binding.mobileNoTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Age
        if (TextUtils.isEmpty(ageStr)) {
            binding.ageTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.ageTextLayout.setError(null);
            isValidAge = true;
        }

        //Email Address
        if (TextUtils.isEmpty(emailStr)) {
            binding.emailTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else if (emailStr.matches(emailPattern)) {
            binding.emailTextLayout.setError(null);
            isValidMailId = true;
        } else {
            binding.emailTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Gender
        if (binding.genderRadioGroup.getCheckedRadioButtonId() == -1) {
            binding.genderErrorTextView.setVisibility(View.VISIBLE);
        } else {
            if (binding.mRadioButton.isChecked()) {
                genderStr = "Male";
                binding.genderErrorTextView.setVisibility(View.GONE);
                isValidGender = true;

            } else if (binding.fRadioButton.isChecked()) {
                genderStr = "Female";
                binding.genderErrorTextView.setVisibility(View.GONE);
                isValidGender = true;
            }
        }

        //Qualification
        if (binding.qualifyRadioGroup.getCheckedRadioButtonId() == -1) {
            binding.qualifiactionErrorTextView.setVisibility(View.VISIBLE);
        } else {
            if (binding.tenRadioButton.isChecked()) {
                qualifyStr = "10th";
                binding.qualifiactionErrorTextView.setVisibility(View.GONE);
                isValidQualification = true;

            } else if (binding.twelveRadioButton.isChecked()) {
                qualifyStr = "12th";
                binding.qualifiactionErrorTextView.setVisibility(View.GONE);
                isValidQualification = true;
            }
        }

        //Address
        if (TextUtils.isEmpty(addressStr)) {
            binding.addressTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.addressTextLayout.setError(null);
            isValidAddress = true;
        }

        //Aadhar
        if (aadharNoStr.length() > 0) {
            if (aadharNoStr.length() < 12) {
                binding.aadharTextLayout.setError(getResources().getString(R.string.enter_valid_aadhar));
            } else {
                isValidAadharNo = true;
                binding.aadharTextLayout.setError(null);
            }
        } else {
            binding.aadharTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //PAN
        if (panNoStr.length() > 0) {
            if (panNoStr.length() < 10) {
                binding.panTextLayout.setError(getResources().getString(R.string.enter_valid_pan));
            } else {
                isValidPanNo = true;
                binding.panTextLayout.setError(null);
            }
        } else {
            binding.panTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Pincode
        if (pincodeStr.length() > 0) {
            if (pincodeStr.length() < 6) {
                binding.pincodeTextLayout.setError(getResources().getString(R.string.enter_valid_pincode));
            } else {
                isValidPincode = true;
                binding.pincodeTextLayout.setError(null);
            }
        } else {
            binding.pincodeTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Password
        if (TextUtils.isEmpty(passwordStr)) {
            binding.passwordTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.passwordTextLayout.setError(null);
            if (TextUtils.isEmpty(confPassStr)) {
                binding.confirmPasswordTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
            } else {
                binding.confirmPasswordTextLayout.setError(null);
                if (!passwordStr.equals(confPassStr)) {
                    binding.passwordTextLayout.setError(getResources().getString(R.string.passwords_do_not_match));
                    binding.confirmPasswordTextLayout.setError(getResources().getString(R.string.passwords_do_not_match));
                } else {
                    if (passwordStr.length() > 0) {
                        if (passwordStr.length() < 5 || passwordStr.length() > 10) {
                            binding.passwordTextLayout.setError(getResources().getString(R.string.enter_valid_pass));
                        } else {
                            isValidPass = true;
                            isValidConfPass = true;
                            binding.passwordTextLayout.setError(null);
                        }
                    }
                }
            }
        }

        //City
        if (cityPos != 0) {
            isValidCity = true;
        }

        //Course
        if (expStr.equals("Yes")) {
            allServiceMonth = binding.courseMonth.getSelectedItemPosition()+"";
            int selMonth = binding.courseMonth.getSelectedItemPosition();
            allServiceYear = binding.courseYear.getText().toString().trim();

            if (TextUtils.isEmpty(allServiceYear)) {
                binding.courseYear.setError(getResources().getString(R.string.field_cannot_be_empty));
            } else {
                binding.courseYear.setError(null);
                isValidCourseYear = true;
            }

            if (selMonth != 0) {
                isValidCourseMonth = true;
            } else {
                binding.courseMonth.setSelection(0);
                Toast.makeText(mCon, "Please select Valid Month", Toast.LENGTH_SHORT).show();
            }

        } else {
            isValidCourseMonth = true;
            isValidCourseYear = true;
        }

        if (isValidName && isValidMailId && isValidMobileNo && isValidAddress && isValidPincode && isValidPass && isValidConfPass && isValidGender && isValidCity && isValidAge
                && isValidAadharNo && isValidPanNo && isValidQualification && isValidExperience && isValidCourseMonth && isValidCourseYear) {

            SignUpModel signUpModel = new SignUpModel(nameStr, emailStr, mobileNoStr, ageStr, qualifyStr, aadharNoStr, panNoStr, genderStr, addressStr,
                    tempAddressStr, selId, cityStr, pincodeStr, passwordStr, aadharFileName, aadharEncodedBase64, panFileName, panEncodedBase64, expStr,
                    "", allServiceMonth, allServiceYear);

            signUpUser(signUpModel);

            Log.d("signUpModel", "" + signUpModel);
        }
    }

    private void signUpUser(SignUpModel signUpModel) {
        try {
            Call<String> call = ApiClient.getNetworkService().signUpUser(signUpModel);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .content("Registration in process")
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Toast.makeText(mCon, "Registration Successful", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            Intent intent = new Intent(mCon, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(mCon, "Data Not Found", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else if (response.code() == 212) {
                        Toast.makeText(mCon, R.string.user_not_found, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } else if (response.code() == ResponseCodes.NOT_ACCEPTABLE) {
                        Toast.makeText(mCon, "Not Acceptable", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } else if (response.code() == ResponseCodes.MOBILE_NUMBER_ALREADY_REGISTERED) {
                        Toast.makeText(mCon, "Mobile number already registered", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        Log.d("check", "" + t.getMessage());
                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "signUpUser: SignUpActivity " + e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (reqCode == SELECT_FILE_AADHAR) {
                Cursor returnCursor = null;

                try {
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        ContentResolver cr = mCon.getContentResolver();
                        String mime = cr.getType(imageUri);

                        returnCursor = mCon.getContentResolver().query(imageUri, null, null, null, null);
                        if (returnCursor != null) {

                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();

                            if (mime != null && mime.equals("image/jpeg") || mime != null && mime.equals("image/jpg")) {

                                if (returnCursor.getLong(sizeIndex) > 2000000) {
                                    Toast.makeText(mCon, "Image size is too large", Toast.LENGTH_SHORT).show();

                                } else {

                                    final InputStream imageStream = mCon.getContentResolver().openInputStream(imageUri);
                                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));

                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }

                                    File insideFolder = new File(dir, getResources().getString(R.string.documents));

                                    if (!insideFolder.exists()) {
                                        insideFolder.mkdirs();
                                    }

                                    File destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");

                                    String strPath = String.valueOf(destination);

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    if (!strPath.equalsIgnoreCase("")) {

                                        if (bytes.size() < 2000000) {

                                            byte[] imageBytes = bytes.toByteArray();

                                            aadharEncodedBase64 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                                            aadharFileName = strPath.substring(strPath.lastIndexOf('/') + 1);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mCon, "Document Type Should be jpeg , jpg , png .", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //   Toast.makeText(mCon, R.string.someThingWentWrong, Toast.LENGTH_SHORT).show();
                } finally {
                    if (returnCursor != null) {
                        returnCursor.close();
                    }
                }

            } else if (reqCode == SELECT_FILE_PAN) {
                Cursor returnCursor = null;

                try {
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        ContentResolver cr = mCon.getContentResolver();
                        String mime = cr.getType(imageUri);

                        returnCursor = mCon.getContentResolver().query(imageUri, null, null, null, null);
                        if (returnCursor != null) {

                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();

                            if (mime != null && mime.equals("image/jpeg") || mime != null && mime.equals("image/jpg")) {

                                if (returnCursor.getLong(sizeIndex) > 2000000) {
                                    Toast.makeText(mCon, "Image size is too large", Toast.LENGTH_SHORT).show();

                                } else {

                                    final InputStream imageStream = mCon.getContentResolver().openInputStream(imageUri);
                                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));

                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }

                                    File insideFolder = new File(dir, getResources().getString(R.string.documents));

                                    if (!insideFolder.exists()) {
                                        insideFolder.mkdirs();
                                    }

                                    File destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");

                                    String strPath = String.valueOf(destination);

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    if (!strPath.equalsIgnoreCase("")) {

                                        if (bytes.size() < 2000000) {

                                            byte[] imageBytes = bytes.toByteArray();

                                            panEncodedBase64 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                                            panFileName = strPath.substring(strPath.lastIndexOf('/') + 1);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mCon, "Document Type Should be jpeg , jpg , png .", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //   Toast.makeText(mCon, R.string.someThingWentWrong, Toast.LENGTH_SHORT).show();
                } finally {
                    if (returnCursor != null) {
                        returnCursor.close();
                    }
                }

            }
        }
    }
}