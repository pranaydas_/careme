package in.dynamicvishva.care4meuser.Signup.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SignUpModel {

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String emailId;

    @SerializedName("mobileno")
    private String mobileNo;

    @SerializedName("age")
    private String age;

    @SerializedName("qualification")
    private String qualification;

    @SerializedName("aadharno")
    private String aadharno;

    @SerializedName("pancardno")
    private String pancardno;

    @SerializedName("gender")
    private String gender;

    @SerializedName("address")
    private String address;

    @SerializedName("tmp_address")
    private String tempAddress;

    @SerializedName("state")
    private int state;

    @SerializedName("city")
    private String city;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("password")
    private String password;

    @SerializedName("aadharDocname")
    private String aadhardocname;

    @SerializedName("aadharBase64")
    private String aadharbase64;

    @SerializedName("panDocname")
    private String pandocname;

    @SerializedName("panBase64")
    private String panbase64;

    @SerializedName("nursing_care")
    private String nursing_care;

    @SerializedName("serviceid")
    private String serviceid;

    @SerializedName("ansmonth")
    private String ansmonth;

    @SerializedName("ansyear")
    private String ansyear;

    public SignUpModel() {
    }

    public SignUpModel(String name, String emailId, String mobileNo, String age, String qualification, String aadharno, String pancardno, String gender, String address, String tempAddress, int state, String city, String pincode, String password, String aadhardocname, String aadharbase64, String pandocname, String panbase64, String nursing_care, String serviceid, String ansmonth, String ansyear) {
        this.name = name;
        this.emailId = emailId;
        this.mobileNo = mobileNo;
        this.age = age;
        this.qualification = qualification;
        this.aadharno = aadharno;
        this.pancardno = pancardno;
        this.gender = gender;
        this.address = address;
        this.tempAddress = tempAddress;
        this.state = state;
        this.city = city;
        this.pincode = pincode;
        this.password = password;
        this.aadhardocname = aadhardocname;
        this.aadharbase64 = aadharbase64;
        this.pandocname = pandocname;
        this.panbase64 = panbase64;
        this.nursing_care = nursing_care;
        this.serviceid = serviceid;
        this.ansmonth = ansmonth;
        this.ansyear = ansyear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getAadharno() {
        return aadharno;
    }

    public void setAadharno(String aadharno) {
        this.aadharno = aadharno;
    }

    public String getPancardno() {
        return pancardno;
    }

    public void setPancardno(String pancardno) {
        this.pancardno = pancardno;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTempAddress() {
        return tempAddress;
    }

    public void setTempAddress(String tempAddress) {
        this.tempAddress = tempAddress;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAadhardocname() {
        return aadhardocname;
    }

    public void setAadhardocname(String aadhardocname) {
        this.aadhardocname = aadhardocname;
    }

    public String getAadharbase64() {
        return aadharbase64;
    }

    public void setAadharbase64(String aadharbase64) {
        this.aadharbase64 = aadharbase64;
    }

    public String getPandocname() {
        return pandocname;
    }

    public void setPandocname(String pandocname) {
        this.pandocname = pandocname;
    }

    public String getPanbase64() {
        return panbase64;
    }

    public void setPanbase64(String panbase64) {
        this.panbase64 = panbase64;
    }

    public String getNursing_care() {
        return nursing_care;
    }

    public void setNursing_care(String nursing_care) {
        this.nursing_care = nursing_care;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getAnsmonth() {
        return ansmonth;
    }

    public void setAnsmonth(String ansmonth) {
        this.ansmonth = ansmonth;
    }

    public String getAnsyear() {
        return ansyear;
    }

    public void setAnsyear(String ansyear) {
        this.ansyear = ansyear;
    }

    @Override
    public String toString() {
        return "SignUpModel{" +
                "name='" + name + '\'' +
                ", emailId='" + emailId + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", age='" + age + '\'' +
                ", qualification='" + qualification + '\'' +
                ", aadharno='" + aadharno + '\'' +
                ", pancardno='" + pancardno + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", tempAddress='" + tempAddress + '\'' +
                ", state=" + state +
                ", city='" + city + '\'' +
                ", pincode='" + pincode + '\'' +
                ", password='" + password + '\'' +
                ", aadhardocname='" + aadhardocname + '\'' +
                ", aadharbase64='" + aadharbase64 + '\'' +
                ", pandocname='" + pandocname + '\'' +
                ", panbase64='" + panbase64 + '\'' +
                ", nursing_care='" + nursing_care + '\'' +
                ", serviceid='" + serviceid + '\'' +
                ", ansmonth='" + ansmonth + '\'' +
                ", ansyear='" + ansyear + '\'' +
                '}';
    }
}
