package in.dynamicvishva.care4meuser.CloudMessaging.Model;

import com.google.gson.annotations.SerializedName;

public class NotificationModel {

    @SerializedName("userId")
    private String userId;

    @SerializedName("fms_key")
    private String fms_key;

    @SerializedName("type")
    private String type;

    public NotificationModel(){}

    public NotificationModel(String userId, String fms_key, String type) {
        this.userId = userId;
        this.fms_key = fms_key;
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFms_key() {
        return fms_key;
    }

    public void setFms_key(String fms_key) {
        this.fms_key = fms_key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "NotificationModel{" +
                "userId='" + userId + '\'' +
                ", fms_key='" + fms_key + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
