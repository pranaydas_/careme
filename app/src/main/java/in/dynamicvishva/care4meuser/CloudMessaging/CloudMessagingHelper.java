package in.dynamicvishva.care4meuser.CloudMessaging;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.iid.FirebaseInstanceId;

import in.dynamicvishva.care4meuser.CloudMessaging.Model.NotificationModel;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CloudMessagingHelper {

    public static void sendCloudMessagingTokenToServer() {

        String cloudMessagingToken = FirebaseInstanceId.getInstance().getToken();
        String userId = PreferenceUtil.getUser().getUserId();

        NotificationModel notificationModel = new NotificationModel(userId, cloudMessagingToken, "Attendant");

        if (cloudMessagingToken != null) {

            Call<String> call = ApiClient.getNetworkService().sendCloudMessagingToken(notificationModel);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                    if (response.isSuccessful()) {
                        Log.d("cloudResponse", ""+response.body());
                        if (response.body() != null && !response.body().equals("")) {
                            PreferenceUtil.setCloudMessagingTokenSent(true);
                        } else {
                            PreferenceUtil.setCloudMessagingTokenSent(false);
                        }
                    } else if (response.code() == 400) {
                        PreferenceUtil.setCloudMessagingTokenSent(false);
                    }

                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    PreferenceUtil.setCloudMessagingTokenSent(false);
                }
            });
        }
    }

}
