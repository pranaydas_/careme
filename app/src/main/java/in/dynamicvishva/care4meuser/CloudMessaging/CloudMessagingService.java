package in.dynamicvishva.care4meuser.CloudMessaging;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.Html;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

import in.dynamicvishva.care4meuser.MainActivity;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;

public class CloudMessagingService extends FirebaseMessagingService {

    private static final String TAG = "CloudMessage";
    String type;

    Bitmap bitmap;
    private String url1;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (PreferenceUtil.getNotification()) {

            Map<String, String> data = remoteMessage.getData();

            if (data != null && data.size() > 0) {

                String type = data.get("type");

                if (type != null && !type.isEmpty()) {
                    Intent intent;

                    switch (type) {
                        case "Update":
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + this.getPackageName()));
                            // CloudMessagingService.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + this.getPackageName())));
                            sendNotification(intent, data.get("title"),
                                    data.get("body"), Boolean.parseBoolean(data.get("priority")));
                            break;

                        default:
                            intent = new Intent(this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra("type", type);

                            sendNotification(intent, data.get("title"),
                                    data.get("body"), Boolean.parseBoolean(data.get("priority")));
                            break;
                    }

                }
            }

        }
    }

    private void sendNotification(Intent intent, String title, String body, boolean priority) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
            }
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //setting user selec
        Uri path = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.sq_logo)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.sq_logo))
                .setTicker(getResources().getString(R.string.app_name))
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSound(path)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(Html.fromHtml(body))
                .setOngoing(priority)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(Html.fromHtml(body)))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        if (notificationManager != null) {
            notificationManager.notify(new Random().nextInt(), mBuilder.build());
        }


    }

}
