package in.dynamicvishva.care4meuser.Dashboard.Adapter;

import android.content.Context;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import in.dynamicvishva.care4meuser.Dashboard.Model.Task;
import in.dynamicvishva.care4meuser.Dashboard.Model.TaskStatusModel;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.databinding.DashboardCardBinding;

public class DashboardTasksAdapter extends RecyclerView.Adapter<DashboardTasksAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context mCon;
    private List<Task> data;
    private ClickListener clickListener;

    public DashboardTasksAdapter(Context context, ClickListener clickListener) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.clickListener = clickListener;
    }

    public void addList(List<Task> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DashboardCardBinding binding = DataBindingUtil.inflate(inflater, R.layout.dashboard_card, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Task current = data.get(position);
        Log.d("currentTasks", ""+current);
        holder.binding.card.setAnimation(AnimationUtils.loadAnimation(mCon, R.anim.slide_from_bottom));

        holder.binding.serviceTitle.setText(current.getSubServiceName());
        holder.binding.serviceTime.setText(new StringBuilder().append(current.getFromTime()).append(" - ").append(current.getToTime()).toString());

        if (current.getWorkStatus() == null)
        {
            Log.d("checkStatus", ""+current.getWorkStatus());
            holder.binding.arrowBtn.setVisibility(View.VISIBLE);
        }
        else if(current.getWorkStatus().equalsIgnoreCase("Complete"))
        {
            Log.d("checkStatus", "Complete");
            holder.binding.card.setBackgroundResource(R.drawable.task_complete);
            holder.binding.arrowBtn.setVisibility(View.GONE);
            holder.binding.editTaskStatus.setVisibility(View.VISIBLE);
        }
        else if (current.getWorkStatus().equalsIgnoreCase("Incomplete"))
        {
            Log.d("checkStatus", "Incomplete");
            holder.binding.card.setBackgroundResource(R.drawable.task_incomplete);
            holder.binding.arrowBtn.setVisibility(View.GONE);
            holder.binding.editTaskStatus.setVisibility(View.VISIBLE);
        }
        else if (current.getWorkStatus().equalsIgnoreCase("Inprogress"))
        {
            Log.d("checkStatus", ""+current.getWorkStatus());
            holder.binding.card.setBackgroundResource(R.drawable.task_inprogress);
            holder.binding.arrowBtn.setVisibility(View.GONE);
            holder.binding.editTaskStatus.setVisibility(View.VISIBLE);
        }

        if (current.getAuto_status().equalsIgnoreCase("Pending")){
            holder.binding.card.setBackgroundResource(R.drawable.task_incomplete);
        }

        holder.binding.arrowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.binding.expandableView.getVisibility() == View.GONE) {
                    TransitionManager.beginDelayedTransition(holder.binding.card, new AutoTransition());
                    holder.binding.expandableView.setVisibility(View.VISIBLE);
                    holder.binding.arrowBtn.setImageResource(R.drawable.ic_up);
                } else {
                    TransitionManager.beginDelayedTransition(holder.binding.card, new AutoTransition());
                    holder.binding.expandableView.setVisibility(View.GONE);
                    holder.binding.arrowBtn.setImageResource(R.drawable.ic_down);
                }
            }
        });

        holder.binding.btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.taskConfirmation("Complete", current.getSrId(), current.getCustomerId());
            }
        });

        holder.binding.btnIncomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.taskConfirmation("Incomplete", current.getSrId(), current.getCustomerId());
            }
        });

        holder.binding.btnInprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.taskConfirmation("Inprogress", current.getSrId(), current.getCustomerId());
            }
        });

        holder.binding.editTaskStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.updateTask(current.getWorkStatus(), current.getSrId(), current.getCustomerId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        DashboardCardBinding binding;

        public MyViewHolder(DashboardCardBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public interface ClickListener{
        void taskConfirmation(String status, String subServiceId, String customerId);

        void updateTask(String workStatus, String srId, String customerId);
    }
}
