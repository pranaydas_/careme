package in.dynamicvishva.care4meuser.Dashboard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Task {

    @SerializedName("srId")
    private String srId;

    @SerializedName("c_buy_service")
    private String cBuyService;

    @SerializedName("customerServiceId")
    private String customerServiceId;

    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("mobileNumber")
    private String mobileNumber;

    @SerializedName("customerId")
    private String customerId;

    @SerializedName("serviceId")
    private String serviceId;

    @SerializedName("subServiceId")
    private String subServiceId;

    @SerializedName("subServiceName")
    private String subServiceName;

    @SerializedName("fromTime")
    private String fromTime;

    @SerializedName("toTime")
    private String toTime;

    @SerializedName("Date")
    private String date;

    @SerializedName("work_status")
    private String workStatus;

    @SerializedName("auto_status")
    private String auto_status;

    public Task(){}

    public Task(String srId, String cBuyService, String customerServiceId, String customerName, String mobileNumber, String customerId, String serviceId, String subServiceId, String subServiceName, String fromTime, String toTime, String date, String workStatus, String auto_status) {
        this.srId = srId;
        this.cBuyService = cBuyService;
        this.customerServiceId = customerServiceId;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.customerId = customerId;
        this.serviceId = serviceId;
        this.subServiceId = subServiceId;
        this.subServiceName = subServiceName;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.date = date;
        this.workStatus = workStatus;
        this.auto_status = auto_status;
    }

    public String getSrId() {
        return srId;
    }

    public void setSrId(String srId) {
        this.srId = srId;
    }

    public String getcBuyService() {
        return cBuyService;
    }

    public void setcBuyService(String cBuyService) {
        this.cBuyService = cBuyService;
    }

    public String getCustomerServiceId() {
        return customerServiceId;
    }

    public void setCustomerServiceId(String customerServiceId) {
        this.customerServiceId = customerServiceId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getSubServiceId() {
        return subServiceId;
    }

    public void setSubServiceId(String subServiceId) {
        this.subServiceId = subServiceId;
    }

    public String getSubServiceName() {
        return subServiceName;
    }

    public void setSubServiceName(String subServiceName) {
        this.subServiceName = subServiceName;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    public String getAuto_status() {
        return auto_status;
    }

    public void setAuto_status(String auto_status) {
        this.auto_status = auto_status;
    }

    @Override
    public String toString() {
        return "Task{" +
                "srId='" + srId + '\'' +
                ", cBuyService='" + cBuyService + '\'' +
                ", customerServiceId='" + customerServiceId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", customerId='" + customerId + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", subServiceId='" + subServiceId + '\'' +
                ", subServiceName='" + subServiceName + '\'' +
                ", fromTime='" + fromTime + '\'' +
                ", toTime='" + toTime + '\'' +
                ", date='" + date + '\'' +
                ", workStatus='" + workStatus + '\'' +
                ", auto_status='" + auto_status + '\'' +
                '}';
    }
}
