package in.dynamicvishva.care4meuser.Dashboard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetailsModel {

    @SerializedName("srId")
    private String srId;

    @SerializedName("userId")
    private String userId;

    @SerializedName("userName")
    private String userName;

    @SerializedName("mobileNumber")
    private String mobileNumber;

    @SerializedName("emailId")
    private String emailId;

    @SerializedName("gender")
    private String gender;

    @SerializedName("stateId")
    private int stateId;

    @SerializedName("cityId")
    private int cityId;

    @SerializedName("address")
    private String address;

    @SerializedName("tmpaddress")
    private String tmpaddress;

    @SerializedName("pincodeNumber")
    private String pincodeNumber;

    @SerializedName("status")
    private String status;

    @SerializedName("age")
    private String age;

    @SerializedName("qualification")
    private String qualification;

    @SerializedName("aadhar")
    private String aadhar;

    @SerializedName("pencard")
    private String pencard;

    @SerializedName("Image")
    private String image;

    @SerializedName("img")
    private String img;

    @SerializedName("aadharimg")
    private String aadharimg;

    @SerializedName("aimg")
    private String aimg;

    @SerializedName("pancardimg")
    private String pancardimg;

    @SerializedName("pimg")
    private String pimg;

    @SerializedName("city")
    private String city;

    @SerializedName("state")
    private String state;

    @SerializedName("nursing_care")
    private String nursing_care;

    @SerializedName("month")
    private String month;

    @SerializedName("year")
    private String year;

    public UserDetailsModel() {
    }

    public UserDetailsModel(String srId, String userId, String userName, String mobileNumber, String emailId, String gender, int stateId, int cityId, String address, String tmpaddress, String pincodeNumber, String status, String age, String qualification, String aadhar, String pencard, String image, String img, String aadharimg, String aimg, String pancardimg, String pimg, String city, String state, String nursing_care, String month, String year) {
        this.srId = srId;
        this.userId = userId;
        this.userName = userName;
        this.mobileNumber = mobileNumber;
        this.emailId = emailId;
        this.gender = gender;
        this.stateId = stateId;
        this.cityId = cityId;
        this.address = address;
        this.tmpaddress = tmpaddress;
        this.pincodeNumber = pincodeNumber;
        this.status = status;
        this.age = age;
        this.qualification = qualification;
        this.aadhar = aadhar;
        this.pencard = pencard;
        this.image = image;
        this.img = img;
        this.aadharimg = aadharimg;
        this.aimg = aimg;
        this.pancardimg = pancardimg;
        this.pimg = pimg;
        this.city = city;
        this.state = state;
        this.nursing_care = nursing_care;
        this.month = month;
        this.year = year;
    }

    public String getSrId() {
        return srId;
    }

    public void setSrId(String srId) {
        this.srId = srId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTmpaddress() {
        return tmpaddress;
    }

    public void setTmpaddress(String tmpaddress) {
        this.tmpaddress = tmpaddress;
    }

    public String getPincodeNumber() {
        return pincodeNumber;
    }

    public void setPincodeNumber(String pincodeNumber) {
        this.pincodeNumber = pincodeNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getPencard() {
        return pencard;
    }

    public void setPencard(String pencard) {
        this.pencard = pencard;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAadharimg() {
        return aadharimg;
    }

    public void setAadharimg(String aadharimg) {
        this.aadharimg = aadharimg;
    }

    public String getAimg() {
        return aimg;
    }

    public void setAimg(String aimg) {
        this.aimg = aimg;
    }

    public String getPancardimg() {
        return pancardimg;
    }

    public void setPancardimg(String pancardimg) {
        this.pancardimg = pancardimg;
    }

    public String getPimg() {
        return pimg;
    }

    public void setPimg(String pimg) {
        this.pimg = pimg;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNursing_care() {
        return nursing_care;
    }

    public void setNursing_care(String nursing_care) {
        this.nursing_care = nursing_care;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "UserDetailsModel{" +
                "srId='" + srId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", emailId='" + emailId + '\'' +
                ", gender='" + gender + '\'' +
                ", stateId=" + stateId +
                ", cityId=" + cityId +
                ", address='" + address + '\'' +
                ", tmpaddress='" + tmpaddress + '\'' +
                ", pincodeNumber='" + pincodeNumber + '\'' +
                ", status='" + status + '\'' +
                ", age='" + age + '\'' +
                ", qualification='" + qualification + '\'' +
                ", aadhar='" + aadhar + '\'' +
                ", pencard='" + pencard + '\'' +
                ", image='" + image + '\'' +
                ", img='" + img + '\'' +
                ", aadharimg='" + aadharimg + '\'' +
                ", aimg='" + aimg + '\'' +
                ", pancardimg='" + pancardimg + '\'' +
                ", pimg='" + pimg + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", nursing_care='" + nursing_care + '\'' +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
