package in.dynamicvishva.care4meuser.Dashboard;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import in.dynamicvishva.care4meuser.CloudMessaging.CloudMessagingHelper;
import in.dynamicvishva.care4meuser.Dashboard.Adapter.DashboardTasksAdapter;
import in.dynamicvishva.care4meuser.Dashboard.Model.Customer;
import in.dynamicvishva.care4meuser.Dashboard.Model.DashboardTasks;
import in.dynamicvishva.care4meuser.Dashboard.Model.Task;
import in.dynamicvishva.care4meuser.Dashboard.Model.TaskStatusModel;
import in.dynamicvishva.care4meuser.Dashboard.Model.UserDetailsModel;
import in.dynamicvishva.care4meuser.Login.LoginActivity;
import in.dynamicvishva.care4meuser.MainActivity;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Review.Model.AddReviewModel;
import in.dynamicvishva.care4meuser.Signup.Model.OnlyService;
import in.dynamicvishva.care4meuser.Signup.Model.StateList;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import in.dynamicvishva.care4meuser.databinding.FragmentDashboardBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    private Context mCon;
    private FragmentDashboardBinding binding;
    private String userId, mobileNo, today, incReason, updateReason, updateStatus, selPhone, popupCustomer;
    private SimpleDateFormat simpleDateFormat;
    private ProgressDialog progressDialog;
    private DashboardTasks dashboardTasks;
    private List<Task> allTasks = new ArrayList<>();
    private UserDetailsModel userDetailsModel;
    private int year, month, day;
    private DashboardTasksAdapter dashboardTasksAdapter;
    private TaskStatusModel taskStatusModel;
    public static List<Customer> customerList;
    private List<String> phoneNumbers;
    public static boolean popupFlag;
    public static int popupFlagCount = 0;
    ArrayAdapter customerAdapter;
    StringBuilder stringBuilder, strBuilder;
    private String title, desc, customers;
    private int stars;

    public DashboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        mCon = getActivity();
        View view = binding.getRoot();
        progressDialog = new ProgressDialog(mCon);

        Window window = getActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));

        if (!PreferenceUtil.getCloudMessagingTokenSent()) {
            CloudMessagingHelper.sendCloudMessagingTokenToServer();
        }

        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        today = day + "-" + (month + 1) + "-" + year;

        if (PreferenceUtil.getUser() != null) {
            userId = PreferenceUtil.getUser().getUserId();
            mobileNo = PreferenceUtil.getUser().getMobileNumber();
            binding.toolbar.setSubtitle(PreferenceUtil.getUser().getUserName());
        } else {
            new Intent(mCon, LoginActivity.class);
        }

        fetchTasksById(userId, today);

        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.dashboardCalender)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                String selectedDate = simpleDateFormat.format(date.getTime());
                fetchTasksById(userId, selectedDate);
            }
        });

        binding.logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceUtil.clearAll();
                startActivity(new Intent(mCon, LoginActivity.class));
                getActivity().finish();
            }
        });

        binding.callAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = "+919773254783";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });

        binding.callCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(customerList != null)
                {
                    if (customerList.size() == 1) {
                        String phone = customerList.get(0).getMobileNumber();
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                        startActivity(intent);
                    } else {
                        customerCallDialog(customerList);
                    }
                }
                else
                {
                    Toast.makeText(mCon, "No customer assigned", Toast.LENGTH_SHORT).show();
                }

            }
        });

        dashboardTasksAdapter = new DashboardTasksAdapter(mCon, new DashboardTasksAdapter.ClickListener() {
            @Override
            public void taskConfirmation(String status, String subServiceId, String custId) {
                confirmationDialog(userId, subServiceId, status, custId);
            }

            @Override
            public void updateTask(String workStatus, String srId, String custId) {
                updateDialog(userId, workStatus, srId, custId);
            }
        });
        binding.tasksRecycler.setLayoutManager(new LinearLayoutManager(mCon));
        binding.tasksRecycler.setHasFixedSize(true);
        binding.tasksRecycler.setItemAnimator(new DefaultItemAnimator());
        return view;
    }

    private void customerCallDialog(List<Customer> customerList) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mCon);
        View view = getLayoutInflater().inflate(R.layout.customer_call_dialog, null);

        final MaterialButton callBtn = view.findViewById(R.id.btnCall);
        final AppCompatSpinner spinner = view.findViewById(R.id.customerPhoneSpinner);

        phoneNumbers = new ArrayList<>();

        for (Customer customer : customerList) {
            phoneNumbers.add(customer.getMobileNumber());
        }

        customerAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, phoneNumbers);
        customerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(customerAdapter);

        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selPhone = spinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", selPhone, null));
                startActivity(intent);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void updateDialog(String userId, String workStatus, String srId, String custId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mCon);
        View view = getLayoutInflater().inflate(R.layout.update_status_dialog, null);

        //final MaterialButton cancelBtn = view.findViewById(R.id.btnCancel);
        final MaterialButton submitBtn = view.findViewById(R.id.btnSubmit);
        final AppCompatSpinner spinner = view.findViewById(R.id.statusSpinner);
        final TextView reasonText = view.findViewById(R.id.reasonText);
        final TextInputLayout textInputLayout = view.findViewById(R.id.reasonLayout);
        final TextInputEditText reason = view.findViewById(R.id.incompleteReason);

        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(mCon, R.array.taskStatusUpdate, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(statusAdapter);

        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

//        cancelBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinner.getItemAtPosition(position).toString().equalsIgnoreCase("Complete")) {
                    updateStatus = "Complete";
                    updateReason = null;
                    reasonText.setVisibility(View.GONE);
                    textInputLayout.setVisibility(View.GONE);

                } else if (spinner.getItemAtPosition(position).toString().equalsIgnoreCase("Inprogress")) {
                    updateStatus = "Inprogress";
                    updateReason = null;
                    reasonText.setVisibility(View.GONE);
                    textInputLayout.setVisibility(View.GONE);
                } else if (spinner.getItemAtPosition(position).toString().equalsIgnoreCase("Incomplete")) {
                    reasonText.setVisibility(View.VISIBLE);
                    textInputLayout.setVisibility(View.VISIBLE);
                    updateStatus = "Incomplete";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textInputLayout.getVisibility() == View.VISIBLE) {
                    if (reason.getText().toString().trim().equals("")) {
                        Toast.makeText(mCon, "Please specify valid reason", Toast.LENGTH_SHORT).show();
                    } else {
                        updateReason = reason.getText().toString().trim();
                        taskStatusModel = new TaskStatusModel(userId, srId, updateStatus, updateReason, custId);
                        updateTaskStatus(taskStatusModel);
                    }
                } else {
                    taskStatusModel = new TaskStatusModel(userId, srId, updateStatus, updateReason, custId);
                    updateTaskStatus(taskStatusModel);
                }
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void fetchTasksById(String userId, String date) {
        progressDialog.setMessage("Fetching Tasks");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<DashboardTasks> call = ApiClient.getNetworkService().fetchDashboardTasks(userId, date);
        call.enqueue(new Callback<DashboardTasks>() {
            @Override
            public void onResponse(Call<DashboardTasks> call, Response<DashboardTasks> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        dashboardTasks = response.body();

                        Log.d("checkTask", "" + dashboardTasks);

                        if (dashboardTasks.getTask() == null) {
                            Toast.makeText(mCon, "Task not found", Toast.LENGTH_SHORT).show();
                            dashboardTasksAdapter.clear();
                            binding.tasksRecycler.setAdapter(dashboardTasksAdapter);

                        } else {
                            allTasks = dashboardTasks.getTask();

                            dashboardTasksAdapter.addList(allTasks);
                            binding.tasksRecycler.setAdapter(dashboardTasksAdapter);
                        }

                        if (dashboardTasks.getCustomer() != null) {
                            customerList = dashboardTasks.getCustomer();
                        }

                        checkPopup(customerList);

                        progressDialog.dismiss();
                    }
                } else if (response.code() == 212) {
                    Toast.makeText(mCon, "Task not found", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else {
                    Log.d("checkDates", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<DashboardTasks> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("checkFail", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void confirmationDialog(String userId, String subServiceId, String status, String custId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mCon);
        View view = getLayoutInflater().inflate(R.layout.confirmation_dialog, null);

        final MaterialButton continueBtn = view.findViewById(R.id.btnContinue);
        final MaterialButton cancelBtn = view.findViewById(R.id.btnCancel);

        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equals("Complete")) {
                    taskStatusModel = new TaskStatusModel(userId, subServiceId, status, null, custId);
                    addTaskStatus(taskStatusModel);
                } else if (status.equals("Inprogress")) {
                    taskStatusModel = new TaskStatusModel(userId, subServiceId, status, null, custId);
                    addTaskStatus(taskStatusModel);
                } else if (status.equals("Incomplete")) {
                    showIncompleteDialog(userId, subServiceId, status, custId);
                }

                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void showIncompleteDialog(String userId, String subServiceId, String status, String custId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mCon);
        View view = getLayoutInflater().inflate(R.layout.incomplete_dialog, null);

        final MaterialButton submitBtn = view.findViewById(R.id.btnSubmit);
        final MaterialButton cancelBtn = view.findViewById(R.id.btnCancel);
        final TextInputEditText reason = view.findViewById(R.id.incompleteReason);

        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reason.getText().toString().trim().equals("")) {
                    Toast.makeText(mCon, "Please write a valid reason", Toast.LENGTH_SHORT).show();
                } else {
                    incReason = reason.getText().toString().trim();
                    taskStatusModel = new TaskStatusModel(userId, subServiceId, status, incReason, custId);
                    addTaskStatus(taskStatusModel);

                    alertDialog.dismiss();
                }
            }
        });

        alertDialog.show();
    }

    private void addTaskStatus(TaskStatusModel taskStatusModel) {
        progressDialog.setMessage("Fetching Tasks");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<String> call = ApiClient.getNetworkService().addTask(taskStatusModel);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            Toast.makeText(mCon, "Task status updated successfully", Toast.LENGTH_SHORT).show();
                            fetchTasksById(userId, today);
                            progressDialog.dismiss();
                        }
                    }
                } else if (response.code() == 406) {
                    Toast.makeText(mCon, "Status update failed", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else if (response.code() == 212) {
                    Toast.makeText(mCon, "Task not found", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else {
                    Log.d("checkDates", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("checkFail", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void updateTaskStatus(TaskStatusModel taskStatusModel) {
        progressDialog.setMessage("Updating Task");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<String> call = ApiClient.getNetworkService().updateTask(taskStatusModel);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            Toast.makeText(mCon, "Task status updated successfully", Toast.LENGTH_SHORT).show();
                            fetchTasksById(userId, today);
                            progressDialog.dismiss();
                        }
                    }
                } else if (response.code() == 406) {
                    Toast.makeText(mCon, "Status update failed", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else if (response.code() == 212) {
                    Toast.makeText(mCon, "Task not found", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else {
                    Log.d("checkDates", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("checkFail", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void checkPopup(List<Customer> customerList) {
        popupCustomer = "";
        stringBuilder = new StringBuilder();

        if(customerList != null)
        {
            if (customerList.size() == 1) {
                popupCustomer = customerList.get(0).getCustomerId();
            } else {
                for (int i = 0; i <= customerList.size() - 1; i++) {
                    if (i == customerList.size() - 1) {
                        popupCustomer = stringBuilder.append(customerList.get(i).getCustomerId()).toString();
                    } else {
                        popupCustomer = stringBuilder.append(customerList.get(i).getCustomerId()).append(",").toString();
                    }
                }
            }
        }

        Call<String> call = ApiClient.getNetworkService().checkPopup(popupCustomer, userId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            popupFlag = true;
                            popupFlagCount += 1;

                            if (popupFlagCount == 1 && popupFlag) {
                                showReviewPopup(userId, customerList);
                            }
                        }
                    }
                } else if (response.code() == 212) {
                } else {
                    Log.d("checkDates", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.d("checkFail", "" + t.getMessage());
                }
            }
        });
    }

    private void showReviewPopup(String userId, List<Customer> customerList) {
        strBuilder = new StringBuilder();

        if (customerList.size() == 1) {
            customers = customerList.get(0).getCustomerId();
        } else {
            for (int i = 0; i <= customerList.size() - 1; i++) {
                if (i == customerList.size() - 1) {
                    customers = stringBuilder.append(customerList.get(i).getCustomerId()).toString();
                } else {
                    customers = stringBuilder.append(customerList.get(i).getCustomerId()).append(",").toString();
                }
            }
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(mCon);
        View view = getLayoutInflater().inflate(R.layout.review_dialog, null);

        final RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        final TextView reviewTitle = view.findViewById(R.id.reviewTitle);
        final TextInputEditText reviewDesc = view.findViewById(R.id.reviewDesc);
        final MaterialButton submitBtn = view.findViewById(R.id.btnSubmit);

        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 1) {
                    reviewTitle.setText("Satisfactory");
                    title = "Satisfactory";
                    stars = 1;
                } else if (rating == 2) {
                    reviewTitle.setText("Good");
                    title = "Good";
                    stars = 2;
                } else if (rating == 3) {
                    reviewTitle.setText("Excellent");
                    title = "Excellent";
                    stars = 3;
                }
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reviewDesc.getText().toString().equals("")) {
                    Toast.makeText(mCon, "Please add description", Toast.LENGTH_SHORT).show();
                } else {
                    desc = reviewDesc.getText().toString().trim();
                    AddReviewModel addReviewModel = new AddReviewModel(customers, userId, title, stars, desc, "User");
                    addReview(addReviewModel);
                }
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void addReview(AddReviewModel addReviewModel) {
        progressDialog.setMessage("Adding Review");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<String> call = ApiClient.getNetworkService().addReview(addReviewModel);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            Toast.makeText(mCon, "Review added Successfully", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();
                    }
                } else if (response.code() == 406) {
                    Toast.makeText(mCon, "Unable to add review", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else {
                    Log.d("checkDates", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("checkFail", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }
}