package in.dynamicvishva.care4meuser.Dashboard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashboardTasks {

    @SerializedName("customer")
    private List<Customer> customer = null;

    @SerializedName("task")
    private List<Task> task = null;

    public DashboardTasks(){}

    public DashboardTasks(List<Customer> customer, List<Task> task) {
        this.customer = customer;
        this.task = task;
    }

    public List<Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(List<Customer> customer) {
        this.customer = customer;
    }

    public List<Task> getTask() {
        return task;
    }

    public void setTask(List<Task> task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "DashboardTasks{" +
                "customer=" + customer +
                ", task=" + task +
                '}';
    }
}
