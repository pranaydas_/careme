package in.dynamicvishva.care4meuser.Dashboard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {

    @SerializedName("srId")
    private String srId;

    @SerializedName("customerId")
    private String customerId;

    @SerializedName("customerName")
    private String customerName;

    @SerializedName("mobileNumber")
    private String mobileNumber;

    @SerializedName("emailId")
    private String emailId;

    @SerializedName("gender")
    private String gender;

    @SerializedName("stateId")
    private String stateId;

    @SerializedName("cityId")
    private String cityId;

    @SerializedName("address")
    private String address;

    @SerializedName("pincodeNumber")
    private String pincodeNumber;

    @SerializedName("status")
    private String status;

    @SerializedName("Image")
    private String image;

    @SerializedName("city")
    private Object city;

    @SerializedName("state")
    private String state;

    public Customer(){}

    public Customer(String srId, String customerId, String customerName, String mobileNumber, String emailId, String gender, String stateId, String cityId, String address, String pincodeNumber, String status, String image, Object city, String state) {
        this.srId = srId;
        this.customerId = customerId;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.emailId = emailId;
        this.gender = gender;
        this.stateId = stateId;
        this.cityId = cityId;
        this.address = address;
        this.pincodeNumber = pincodeNumber;
        this.status = status;
        this.image = image;
        this.city = city;
        this.state = state;
    }

    public String getSrId() {
        return srId;
    }

    public void setSrId(String srId) {
        this.srId = srId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincodeNumber() {
        return pincodeNumber;
    }

    public void setPincodeNumber(String pincodeNumber) {
        this.pincodeNumber = pincodeNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "srId='" + srId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", emailId='" + emailId + '\'' +
                ", gender='" + gender + '\'' +
                ", stateId='" + stateId + '\'' +
                ", cityId='" + cityId + '\'' +
                ", address='" + address + '\'' +
                ", pincodeNumber='" + pincodeNumber + '\'' +
                ", status='" + status + '\'' +
                ", image='" + image + '\'' +
                ", city=" + city +
                ", state='" + state + '\'' +
                '}';
    }
}
