package in.dynamicvishva.care4meuser.Dashboard.Model;

import com.google.gson.annotations.SerializedName;

public class TaskStatusModel {

    @SerializedName("Userid")
    private String userId;

    @SerializedName("Taskid")
    private String taskId;

    @SerializedName("Workstatus")
    private String workStatus;

    @SerializedName("comment")
    private String comment;

    @SerializedName("customerid")
    private String customerid;

    public TaskStatusModel(){}

    public TaskStatusModel(String userId, String taskId, String workStatus, String comment, String customerid) {
        this.userId = userId;
        this.taskId = taskId;
        this.workStatus = workStatus;
        this.comment = comment;
        this.customerid = customerid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    @Override
    public String toString() {
        return "TaskStatusModel{" +
                "userId='" + userId + '\'' +
                ", taskId='" + taskId + '\'' +
                ", workStatus='" + workStatus + '\'' +
                ", comment='" + comment + '\'' +
                ", customerid='" + customerid + '\'' +
                '}';
    }
}
