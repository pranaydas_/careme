package in.dynamicvishva.care4meuser.Profile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.dynamicvishva.care4meuser.Dashboard.Model.Customer;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.databinding.CustomerInfoCardBinding;

public class CustomerDetailsAdapter extends RecyclerView.Adapter<CustomerDetailsAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context mCon;
    private List<Customer> data;

    public CustomerDetailsAdapter(Context context) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
    }

    public void addList(List<Customer> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    private void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomerInfoCardBinding binding = DataBindingUtil.inflate(inflater, R.layout.customer_info_card, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Customer current = data.get(position);

        holder.binding.card.setAnimation(AnimationUtils.loadAnimation(mCon, R.anim.slide_from_bottom));
        holder.binding.name.setText(current.getCustomerName());
        holder.binding.phoneNumber.setText(current.getMobileNumber());
        holder.binding.address.setText(current.getAddress());

        Glide
                .with(mCon)
                .load(current.getImage())
                .centerCrop()
                .into(holder.binding.profilePic);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CustomerInfoCardBinding binding;

        public MyViewHolder(CustomerInfoCardBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
