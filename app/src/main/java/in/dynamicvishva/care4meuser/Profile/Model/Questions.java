package in.dynamicvishva.care4meuser.Profile.Model;

import com.google.gson.annotations.SerializedName;

public class Questions {

    @SerializedName("month")
    private String month;

    @SerializedName("year")
    private String year;

    public Questions(){}

    public Questions(String month, String year) {
        this.month = month;
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Questions{" +
                "month='" + month + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
