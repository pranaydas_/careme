package in.dynamicvishva.care4meuser.Profile.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.dynamicvishva.care4meuser.Dashboard.Model.UserDetailsModel;
import in.dynamicvishva.care4meuser.Login.LoginActivity;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.Profile.Model.Questions;
import in.dynamicvishva.care4meuser.Profile.Model.UserExperience;
import in.dynamicvishva.care4meuser.Profile.Model.UserUpdateModel;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Signup.Model.CityList;
import in.dynamicvishva.care4meuser.Signup.Model.OnlyService;
import in.dynamicvishva.care4meuser.Signup.Model.StateList;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import in.dynamicvishva.care4meuser.databinding.ActivityEditProfileBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends AppCompatActivity {

    private Context mCon;
    private ActivityEditProfileBinding binding;
    private String userId, mobileNo;
    private UserDetailsModel userDetailsModel;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String nameStr, emailStr, addressStr, pincodeStr, cityStr, stateStr, tempAddressStr, oldAadharImageStr, oldPanImageStr, expStr, cityOldStr="";
    private String oldImageStr, encodedBase64, genderStr, fileName, ageStr, aadharNoStr, panNoStr, qualifyStr, allServiceId, allServiceMonth="", allServiceYear="";
    private int statePos, cityPos, selId;
    private List<String> states, cities, ansYear, ansMonth;
    private List<Integer> statesIdDb;
    ArrayAdapter stateAdapter, cityAdapter;
    private String aadharEncodedBase64, aadharFileName, panEncodedBase64, panFileName;
    private int REQUEST_CAMERA = 1, SELECT_FILE_AADHAR = 10, SELECT_FILE_PAN = 11, SELECT_FILE = 0;
    private static int MY_PERMISSIONS_REQUEST_CAMERA = 11;
    private static int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 12;
    private List<StateList> stateList = new ArrayList<>();
    private List<CityList> cityList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private boolean isValidExperience = false;
    private List<UserExperience> userExperienceList = new ArrayList<>();
    ArrayAdapter monthAdapter;
    private String[] months;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        mCon = this;
        progressDialog = new ProgressDialog(mCon);

        if (PreferenceUtil.getUser() != null) {
            userId = PreferenceUtil.getUser().getUserId();
            mobileNo = PreferenceUtil.getUser().getMobileNumber();
        } else {
            new Intent(mCon, LoginActivity.class);
        }

        fetchUserDetails(mobileNo);

        binding.profilePicSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mCon,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

                } else if (ContextCompat.checkSelfPermission(mCon,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

                } else {
                    galleryIntent();
                }
            }
        });

        binding.aadharPicSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mCon,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

                } else if (ContextCompat.checkSelfPermission(mCon,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

                } else {
                    aadharIntent();
                }
            }
        });

        binding.panPicSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mCon,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

                } else if (ContextCompat.checkSelfPermission(mCon,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

                } else {
                    panIntent();
                }
            }
        });

        //Experience
        if (binding.experienceRadioGroup.getCheckedRadioButtonId() == -1) {
            binding.experienceErrorTextView.setVisibility(View.VISIBLE);
        } else {
            binding.experienceRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (binding.yesRadioButton.isChecked()) {
                        expStr = "Yes";
                        binding.experienceErrorTextView.setVisibility(View.GONE);
                        isValidExperience = true;
                        binding.layout1.setVisibility(View.VISIBLE);
                        binding.layout2.setVisibility(View.VISIBLE);
                    } else if (binding.noRadioButton.isChecked()) {
                        expStr = "No";
                        binding.experienceErrorTextView.setVisibility(View.GONE);
                        isValidExperience = true;
                        binding.layout1.setVisibility(View.GONE);
                        binding.layout2.setVisibility(View.GONE);
                    }
                }
            });
        }

        binding.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameStr = binding.nameEditText.getText().toString().trim();
                emailStr = binding.emailEditText.getText().toString().trim();
                addressStr = binding.addressEditText.getText().toString().trim();
                tempAddressStr = binding.tempAddressEditText.getText().toString().trim();
                pincodeStr = binding.pincodeEditText.getText().toString().trim();
                ageStr = binding.ageEditText.getText().toString();
                aadharNoStr = binding.aadharEditText.getText().toString();
                panNoStr = binding.panEditText.getText().toString();

                validate();
            }
        });
    }

    private void loadStateNew() {
        try {
            Call<List<StateList>> call = ApiClient.getNetworkService().fetchStates();

            call.enqueue(new Callback<List<StateList>>() {
                @Override
                public void onResponse(Call<List<StateList>> call, Response<List<StateList>> response) {
                    if (response.isSuccessful()) {
                        stateList = response.body();

                        if (stateList != null) {

                            states = new ArrayList<>();
                            statesIdDb = new ArrayList<>();
                            states.add("Select State");
                            statesIdDb.add(0);

                            for (StateList model : stateList) {
                                states.add(model.getStateName());
                                statesIdDb.add(model.getStateId());
                            }

                            stateAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, states);
                            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.stateSpinner.setAdapter(stateAdapter);

                            binding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    stateStr = binding.stateSpinner.getSelectedItem().toString();
                                    statePos = binding.stateSpinner.getSelectedItemPosition();

                                    selId = statesIdDb.get(statePos);

                                    loadCities(selId);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<StateList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadState(int stateId) {
        try {
            Call<List<StateList>> call = ApiClient.getNetworkService().fetchStates();

            call.enqueue(new Callback<List<StateList>>() {
                @Override
                public void onResponse(Call<List<StateList>> call, Response<List<StateList>> response) {
                    if (response.isSuccessful()) {
                        stateList = response.body();

                        if (stateList != null) {

                            states = new ArrayList<>();
                            statesIdDb = new ArrayList<>();
                            states.add("Select State");
                            statesIdDb.add(0);

                            for (StateList model : stateList) {
                                states.add(model.getStateName());
                                statesIdDb.add(model.getStateId());
                            }

                            stateAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, states);
                            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.stateSpinner.setAdapter(stateAdapter);

                            for (int k = 0; k <= statesIdDb.size() - 1; k++) {
                                if (statesIdDb.get(k) == stateId) {
                                    binding.stateSpinner.setSelection(k);
                                }
                            }

                            binding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    stateStr = binding.stateSpinner.getSelectedItem().toString();
                                    statePos = binding.stateSpinner.getSelectedItemPosition();

                                    selId = statesIdDb.get(statePos);

                                    loadCities(selId);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<StateList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCities(int stateId) {
        try {
            Call<List<CityList>> call = ApiClient.getNetworkService().fetchCities(stateId);

            call.enqueue(new Callback<List<CityList>>() {
                @Override
                public void onResponse(Call<List<CityList>> call, Response<List<CityList>> response) {
                    if (response.isSuccessful()) {
                        cityList = response.body();

                        if (cityList != null) {

                            cities = new ArrayList<>();
                            cities.add("Select City");

                            for (CityList model : cityList) {
                                cities.add(model.getCityName());
                            }

                            cityAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, cities);
                            cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.citySpinner.setAdapter(cityAdapter);

                            if (!cityOldStr.equalsIgnoreCase("")){
                                String compareValue = cityOldStr;
                                if (compareValue != null) {
                                    int spinnerPosition = cityAdapter.getPosition(compareValue);
                                    binding.citySpinner.setSelection(spinnerPosition);
                                }
                            }

                            binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    cityStr = binding.citySpinner.getSelectedItem().toString();
                                    cityPos = binding.citySpinner.getSelectedItemPosition();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    }
                }

                @Override
                public void onFailure(Call<List<CityList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryIntent() {
        String[] mimeTypes = {"image/jpeg", "image/jpg"};
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), SELECT_FILE);
    }

    private void fetchUserDetails(String mobileNo) {

        progressDialog.setMessage("Fetching Details");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<UserDetailsModel> call = ApiClient.getNetworkService().fetchUserDetails(mobileNo);
        call.enqueue(new Callback<UserDetailsModel>() {
            @Override
            public void onResponse(Call<UserDetailsModel> call, Response<UserDetailsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        userDetailsModel = response.body();

                        binding.nameEditText.setText(userDetailsModel.getUserName());
                        binding.emailEditText.setText(userDetailsModel.getEmailId());
                        binding.ageEditText.setText(userDetailsModel.getAge());
                        if (userDetailsModel.getGender().equals("Male")) {
                            binding.mRadioButton.setChecked(true);
                        } else if (userDetailsModel.getGender().equals("Female")) {
                            binding.fRadioButton.setChecked(true);
                        }
                        binding.addressEditText.setText(userDetailsModel.getAddress());
                        binding.tempAddressEditText.setText(userDetailsModel.getTmpaddress());
                        if (userDetailsModel.getQualification().equals("10th")) {
                            binding.tenRadioButton.setChecked(true);
                        } else if (userDetailsModel.getQualification().equals("12th")) {
                            binding.twelveRadioButton.setChecked(true);
                        }
                        binding.pincodeEditText.setText(userDetailsModel.getPincodeNumber());
                        binding.aadharEditText.setText(userDetailsModel.getAadhar());
                        binding.panEditText.setText(userDetailsModel.getPencard());
                        if (userDetailsModel.getNursing_care().equals("Yes")) {
                            binding.yesRadioButton.setChecked(true);
                            binding.layout1.setVisibility(View.VISIBLE);
                            binding.layout2.setVisibility(View.VISIBLE);
                            binding.courseYear.setText(userDetailsModel.getYear());
                            binding.courseMonth.setSelection(Integer.parseInt(userDetailsModel.getMonth()));
                        } else if (userDetailsModel.getNursing_care().equals("No")) {
                            binding.noRadioButton.setChecked(true);
                            binding.layout1.setVisibility(View.GONE);
                            binding.layout2.setVisibility(View.GONE);
                        }
                        if (userDetailsModel.getCity() != null && !(userDetailsModel.getCity().equalsIgnoreCase(""))) {
                            cityOldStr = userDetailsModel.getCity();
                        }
                        if (userDetailsModel.getStateId() != 0) {
                            loadState(userDetailsModel.getStateId());
                        } else {
                            loadStateNew();
                            binding.stateSpinner.setSelection(0);
                        }

                        if (userDetailsModel.getImage() != null) {
                            oldImageStr = userDetailsModel.getImg();
                        }

                        if (userDetailsModel.getAadharimg() != null) {
                            oldAadharImageStr = userDetailsModel.getAimg();
                        }

                        if (userDetailsModel.getPancardimg() != null) {
                            oldPanImageStr = userDetailsModel.getPimg();
                        }

                        progressDialog.dismiss();
                    }
                } else if (response.code() == 401) {
                    PreferenceUtil.clearAll();
                    mCon.startActivity(new Intent(mCon, LoginActivity.class));
                    ((Activity) mCon).finish();
                    progressDialog.dismiss();
                } else {
                    Log.d("check", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<UserDetailsModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("check", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void aadharIntent() {
        String[] mimeTypes = {"image/jpeg", "image/jpg"};
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), SELECT_FILE_AADHAR);
    }

    private void panIntent() {
        String[] mimeTypes = {"image/jpeg", "image/jpg"};
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), SELECT_FILE_PAN);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (reqCode == SELECT_FILE) {
                Cursor returnCursor = null;

                try {
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        ContentResolver cr = mCon.getContentResolver();
                        String mime = cr.getType(imageUri);

                        returnCursor = mCon.getContentResolver().query(imageUri, null, null, null, null);
                        if (returnCursor != null) {

                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();

                            if (mime != null && mime.equals("image/jpeg") || mime != null && mime.equals("image/jpg")) {

                                if (returnCursor.getLong(sizeIndex) > 2000000) {
                                    Toast.makeText(mCon, "Image size is too large", Toast.LENGTH_SHORT).show();

                                } else {

                                    final InputStream imageStream = mCon.getContentResolver().openInputStream(imageUri);
                                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));

                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }

                                    File insideFolder = new File(dir, getResources().getString(R.string.documents));

                                    if (!insideFolder.exists()) {
                                        insideFolder.mkdirs();
                                    }

                                    File destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");

                                    String strPath = String.valueOf(destination);

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    if (!strPath.equalsIgnoreCase("")) {

                                        if (bytes.size() < 2000000) {

                                            byte[] imageBytes = bytes.toByteArray();
                                            encodedBase64 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                                            fileName = strPath.substring(strPath.lastIndexOf('/') + 1);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mCon, "Document Type Should be jpeg , jpg , png .", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //   Toast.makeText(mCon, R.string.someThingWentWrong, Toast.LENGTH_SHORT).show();
                } finally {
                    if (returnCursor != null) {
                        returnCursor.close();
                    }
                }

            } else if (reqCode == SELECT_FILE_AADHAR) {
                Cursor returnCursor = null;

                try {
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        ContentResolver cr = mCon.getContentResolver();
                        String mime = cr.getType(imageUri);

                        returnCursor = mCon.getContentResolver().query(imageUri, null, null, null, null);
                        if (returnCursor != null) {

                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();

                            if (mime != null && mime.equals("image/jpeg") || mime != null && mime.equals("image/jpg")) {

                                if (returnCursor.getLong(sizeIndex) > 2000000) {
                                    Toast.makeText(mCon, "Image size is too large", Toast.LENGTH_SHORT).show();

                                } else {

                                    final InputStream imageStream = mCon.getContentResolver().openInputStream(imageUri);
                                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));

                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }

                                    File insideFolder = new File(dir, getResources().getString(R.string.documents));

                                    if (!insideFolder.exists()) {
                                        insideFolder.mkdirs();
                                    }

                                    File destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");

                                    String strPath = String.valueOf(destination);

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    if (!strPath.equalsIgnoreCase("")) {

                                        if (bytes.size() < 2000000) {

                                            byte[] imageBytes = bytes.toByteArray();

                                            aadharEncodedBase64 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                                            aadharFileName = strPath.substring(strPath.lastIndexOf('/') + 1);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mCon, "Document Type Should be jpeg , jpg , png .", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //   Toast.makeText(mCon, R.string.someThingWentWrong, Toast.LENGTH_SHORT).show();
                } finally {
                    if (returnCursor != null) {
                        returnCursor.close();
                    }
                }

            } else if (reqCode == SELECT_FILE_PAN) {
                Cursor returnCursor = null;

                try {
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        ContentResolver cr = mCon.getContentResolver();
                        String mime = cr.getType(imageUri);

                        returnCursor = mCon.getContentResolver().query(imageUri, null, null, null, null);
                        if (returnCursor != null) {

                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();

                            if (mime != null && mime.equals("image/jpeg") || mime != null && mime.equals("image/jpg")) {

                                if (returnCursor.getLong(sizeIndex) > 2000000) {
                                    Toast.makeText(mCon, "Image size is too large", Toast.LENGTH_SHORT).show();

                                } else {

                                    final InputStream imageStream = mCon.getContentResolver().openInputStream(imageUri);
                                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));

                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }

                                    File insideFolder = new File(dir, getResources().getString(R.string.documents));

                                    if (!insideFolder.exists()) {
                                        insideFolder.mkdirs();
                                    }

                                    File destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");

                                    String strPath = String.valueOf(destination);

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    if (!strPath.equalsIgnoreCase("")) {

                                        if (bytes.size() < 2000000) {

                                            byte[] imageBytes = bytes.toByteArray();

                                            panEncodedBase64 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                                            panFileName = strPath.substring(strPath.lastIndexOf('/') + 1);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mCon, "Document Type Should be jpeg , jpg , png .", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //   Toast.makeText(mCon, R.string.someThingWentWrong, Toast.LENGTH_SHORT).show();
                } finally {
                    if (returnCursor != null) {
                        returnCursor.close();
                    }
                }

            }
        }
    }

    private void validate() {
        boolean isValidName = false, isValidMailId = false, isValidAddress = false, isValidPincode = false,
                isValidGender = false, isValidAge = false, isValidCity = false,
                isValidAadharNo = false, isValidPanNo = false, isValidQualification = false, isValidCourseMonth = false,
                isValidCourseYear = false;

        //Name
        if (TextUtils.isEmpty(nameStr)) {
            binding.nameTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.nameTextLayout.setError(null);
            isValidName = true;
        }

        //Age
        if (TextUtils.isEmpty(ageStr)) {
            binding.ageTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.ageTextLayout.setError(null);
            isValidAge = true;
        }

        //Email Address
        if (TextUtils.isEmpty(emailStr)) {
            binding.emailTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else if (emailStr.matches(emailPattern)) {
            binding.emailTextLayout.setError(null);
            isValidMailId = true;
        } else {
            binding.emailTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Gender
        if (binding.genderRadioGroup.getCheckedRadioButtonId() == -1) {
            binding.genderErrorTextView.setVisibility(View.VISIBLE);
        } else {
            if (binding.mRadioButton.isChecked()) {
                genderStr = "Male";
                binding.genderErrorTextView.setVisibility(View.GONE);
                isValidGender = true;

            } else if (binding.fRadioButton.isChecked()) {
                genderStr = "Female";
                binding.genderErrorTextView.setVisibility(View.GONE);
                isValidGender = true;
            }
        }

        //Qualification
        if (binding.qualifyRadioGroup.getCheckedRadioButtonId() == -1) {
            binding.qualifiactionErrorTextView.setVisibility(View.VISIBLE);
        } else {
            if (binding.tenRadioButton.isChecked()) {
                qualifyStr = "10th";
                binding.qualifiactionErrorTextView.setVisibility(View.GONE);
                isValidQualification = true;

            } else if (binding.twelveRadioButton.isChecked()) {
                qualifyStr = "12th";
                binding.qualifiactionErrorTextView.setVisibility(View.GONE);
                isValidQualification = true;
            }
        }

        //Address
        if (TextUtils.isEmpty(addressStr)) {
            binding.addressTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.addressTextLayout.setError(null);
            isValidAddress = true;
        }

        //Aadhar
        if (aadharNoStr.length() > 0) {
            if (aadharNoStr.length() < 12) {
                binding.aadharTextLayout.setError(getResources().getString(R.string.enter_valid_aadhar));
            } else {
                isValidAadharNo = true;
                binding.aadharTextLayout.setError(null);
            }
        } else {
            binding.aadharTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //PAN
        if (panNoStr.length() > 0) {
            if (panNoStr.length() < 10) {
                binding.panTextLayout.setError(getResources().getString(R.string.enter_valid_pan));
            } else {
                isValidPanNo = true;
                binding.panTextLayout.setError(null);
            }
        } else {
            binding.panTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Pincode
        if (pincodeStr.length() > 0) {
            if (pincodeStr.length() < 6) {
                binding.pincodeTextLayout.setError(getResources().getString(R.string.enter_valid_pincode));
            } else {
                isValidPincode = true;
                binding.pincodeTextLayout.setError(null);
            }
        } else {
            binding.pincodeTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //City
        if (cityPos != 0) {
            isValidCity = true;
        }

        //Course
        if (expStr.equals("Yes")) {
            allServiceMonth = binding.courseMonth.getSelectedItemPosition()+"";
            int selMonth = binding.courseMonth.getSelectedItemPosition();
            allServiceYear = binding.courseYear.getText().toString().trim();

            if (TextUtils.isEmpty(allServiceYear)) {
                binding.courseYear.setError(getResources().getString(R.string.field_cannot_be_empty));
            } else {
                binding.courseYear.setError(null);
                isValidCourseYear = true;
            }

            if (selMonth != 0) {
                isValidCourseMonth = true;
            } else {
                binding.courseMonth.setSelection(0);
                Toast.makeText(mCon, "Please select Valid Month", Toast.LENGTH_SHORT).show();
            }

        } else {
            isValidCourseMonth = true;
            isValidCourseYear = true;
        }

        if (isValidName && isValidMailId && isValidAddress && isValidPincode && isValidGender && isValidCity && isValidAge
                && isValidAadharNo && isValidPanNo && isValidQualification && isValidExperience && isValidCourseMonth && isValidCourseYear)
        {
            UserUpdateModel userUpdateModel = new UserUpdateModel(userId, nameStr, emailStr, mobileNo, ageStr, qualifyStr, aadharNoStr,
                    panNoStr, genderStr, addressStr, tempAddressStr, selId, cityStr, pincodeStr, oldImageStr,
                    oldAadharImageStr, oldPanImageStr, fileName, encodedBase64, aadharFileName, aadharEncodedBase64, panFileName, panEncodedBase64,
                    expStr, "", allServiceMonth, allServiceYear);
            updateUser(userUpdateModel);

            Log.d("userUpdateModel", "" + userUpdateModel);
        }
    }

    private void updateUser(UserUpdateModel userUpdateModel) {
        progressDialog.setMessage("Updating Profile");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<String> call = ApiClient.getNetworkService().updateUser(userUpdateModel);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(mCon, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                        progressDialog.dismiss();
                    }
                } else if (response.code() == 406) {
                    Toast.makeText(mCon, "Profile update failed", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else if (response.code() == 401) {
                    PreferenceUtil.clearAll();
                    mCon.startActivity(new Intent(mCon, LoginActivity.class));
                    ((Activity) mCon).finish();
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    Log.d("check", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("check", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }
}