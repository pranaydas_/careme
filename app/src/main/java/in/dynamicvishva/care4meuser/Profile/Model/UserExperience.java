package in.dynamicvishva.care4meuser.Profile.Model;

import com.google.gson.annotations.SerializedName;

public class UserExperience {

    @SerializedName("serviceId")
    private String serviceId;

    @SerializedName("serviceName")
    private String serviceName;

    @SerializedName("month")
    private String month;

    @SerializedName("year")
    private String year;

    public UserExperience(){}

    public UserExperience(String serviceId, String serviceName, String month, String year) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.month = month;
        this.year = year;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "UserExperience{" +
                "serviceId='" + serviceId + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}
