package in.dynamicvishva.care4meuser.Profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.List;

import in.dynamicvishva.care4meuser.Dashboard.Model.UserDetailsModel;
import in.dynamicvishva.care4meuser.Login.LoginActivity;
import in.dynamicvishva.care4meuser.MainActivity;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.Profile.Activities.CustomerInfo;
import in.dynamicvishva.care4meuser.Profile.Activities.EditProfile;
import in.dynamicvishva.care4meuser.Profile.Model.UserExperience;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import in.dynamicvishva.care4meuser.databinding.FragmentProfileBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends Fragment {

    private Context mCon;
    private FragmentProfileBinding binding;
    private String userId, mobileNo;
    private UserDetailsModel userDetailsModel;
    private List<UserExperience> userExperienceList;
    private ProgressDialog progressDialog;

    public ProfileFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.upper_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionInfo:
                startActivity(new Intent(mCon, CustomerInfo.class));
                break;
            case R.id.actionEditProfile:
                startActivity(new Intent(mCon, EditProfile.class));
                break;
            case R.id.actionLogout:
                PreferenceUtil.clearAll();
                startActivity(new Intent(mCon, LoginActivity.class));
                getActivity().finish();
                break;
        }

        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mCon = getActivity();
        View view = binding.getRoot();
        progressDialog = new ProgressDialog(mCon);

        ((MainActivity) getActivity()).setSupportActionBar(binding.profileToolbar);
        setHasOptionsMenu(true);

        Window window = getActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));

        if (PreferenceUtil.getUser() != null) {
            userId = PreferenceUtil.getUser().getUserId();
            mobileNo = PreferenceUtil.getUser().getMobileNumber();
            fetchUserDetails(mobileNo);
        } else {
            new Intent(mCon, LoginActivity.class);
        }

        return view;
    }

    private void fetchUserDetails(String mobileNo) {
        progressDialog.setMessage("Fetching Details");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<UserDetailsModel> call = ApiClient.getNetworkService().fetchUserDetails(mobileNo);
        call.enqueue(new Callback<UserDetailsModel>() {
            @Override
            public void onResponse(Call<UserDetailsModel> call, Response<UserDetailsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            userDetailsModel = response.body();
                            binding.userName.setText(userDetailsModel.getUserName());
                            binding.userEmail.setText(userDetailsModel.getEmailId());
                            binding.userContact.setText(userDetailsModel.getMobileNumber());
                            binding.userGender.setText(userDetailsModel.getGender());
                            binding.userAddress.setText(userDetailsModel.getAddress());
                            binding.userPincode.setText(userDetailsModel.getPincodeNumber());
                            binding.userState.setText(userDetailsModel.getState());
                            binding.userCity.setText(userDetailsModel.getCity());
                            binding.userAge.setText(userDetailsModel.getAge());
                            binding.userAadhar.setText(userDetailsModel.getAadhar());
                            binding.userPan.setText(userDetailsModel.getPencard());
                            binding.userQualification.setText(userDetailsModel.getQualification());
                            binding.userExperience.setText(userDetailsModel.getNursing_care());

                            if (userDetailsModel.getImage() != null) {
                                Glide
                                        .with(mCon)
                                        .load(userDetailsModel.getImage())
                                        .centerCrop()
                                        .into(binding.profilePic);
                            }

                            if (userDetailsModel.getAadharimg() != null) {
                                Glide
                                        .with(mCon)
                                        .load(userDetailsModel.getAadharimg())
                                        .centerCrop()
                                        .into(binding.aadharPic);
                            } else {
                                binding.noAadhar.setVisibility(View.VISIBLE);
                            }

                            if (userDetailsModel.getPancardimg() != null) {
                                Glide
                                        .with(mCon)
                                        .load(userDetailsModel.getPancardimg())
                                        .centerCrop()
                                        .into(binding.panPic);
                            } else {
                                binding.noPan.setVisibility(View.VISIBLE);
                            }

                            progressDialog.dismiss();
                        } else if (response.code() == 212) {
                            Toast.makeText(mCon, "Failed to load profile", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                } else if (response.code() == 404) {
                    Toast.makeText(mCon, "Not found", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else if (response.code() == 401) {
                    PreferenceUtil.clearAll();
                    mCon.startActivity(new Intent(mCon, LoginActivity.class));
                    ((Activity) mCon).finish();
                    progressDialog.dismiss();
                } else {
                    Log.d("check", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<UserDetailsModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("check", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (PreferenceUtil.getUser() != null) {
            userId = PreferenceUtil.getUser().getUserId();
            mobileNo = PreferenceUtil.getUser().getMobileNumber();
            fetchUserDetails(mobileNo);
        }
    }
}