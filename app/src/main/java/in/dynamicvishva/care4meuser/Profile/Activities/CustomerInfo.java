package in.dynamicvishva.care4meuser.Profile.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import in.dynamicvishva.care4meuser.Dashboard.DashboardFragment;
import in.dynamicvishva.care4meuser.Dashboard.Model.Customer;
import in.dynamicvishva.care4meuser.Login.LoginActivity;
import in.dynamicvishva.care4meuser.MainActivity;
import in.dynamicvishva.care4meuser.Profile.Adapter.CustomerDetailsAdapter;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Review.Adapter.ReviewAdapter;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import in.dynamicvishva.care4meuser.databinding.ActivityCustomerInfoBinding;

public class CustomerInfo extends AppCompatActivity {

    private Context mCon;
    private ActivityCustomerInfoBinding binding;
    private String userId, mobileNo;
    private ProgressDialog progressDialog;
    private CustomerDetailsAdapter customerDetailsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_customer_info);
        mCon = this;
        View view = binding.getRoot();
        progressDialog = new ProgressDialog(mCon);

        if (PreferenceUtil.getUser() != null) {
            userId = PreferenceUtil.getUser().getUserId();
            mobileNo = PreferenceUtil.getUser().getMobileNumber();
        } else {
            new Intent(mCon, LoginActivity.class);
        }

        binding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        List<Customer> customerList = DashboardFragment.customerList;
        customerDetailsAdapter = new CustomerDetailsAdapter(mCon);
        binding.customerRecycler.setLayoutManager(new LinearLayoutManager(mCon));
        binding.customerRecycler.setHasFixedSize(true);
        binding.customerRecycler.setItemAnimator(new DefaultItemAnimator());

        if(customerList != null)
        {
            customerDetailsAdapter.addList(customerList);
            binding.customerRecycler.setAdapter(customerDetailsAdapter);
        }
        else
        {
            Toast.makeText(mCon, "No customers assigned", Toast.LENGTH_SHORT).show();
        }

    }
}