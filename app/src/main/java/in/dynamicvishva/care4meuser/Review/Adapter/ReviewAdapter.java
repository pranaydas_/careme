package in.dynamicvishva.care4meuser.Review.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Review.Model.ReviewModel;
import in.dynamicvishva.care4meuser.databinding.ReviewCardBinding;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context mCon;
    private List<ReviewModel> data;

    public ReviewAdapter(Context context) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
    }

    public void addList(List<ReviewModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    private void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReviewCardBinding binding = DataBindingUtil.inflate(inflater, R.layout.review_card, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ReviewModel current = data.get(position);
        Log.d("review", ""+current);

        holder.binding.card.setAnimation(AnimationUtils.loadAnimation(mCon, R.anim.slide_from_bottom));
        holder.binding.reviewDate.setText(current.getDate());
        holder.binding.reviewDescription.setText(current.getDiscription());
        holder.binding.reviewTitle.setText(current.getTitle());
        holder.binding.ratingBar.setRating(current.getStar());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ReviewCardBinding binding;

        public MyViewHolder(ReviewCardBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
