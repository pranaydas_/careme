package in.dynamicvishva.care4meuser.Review.Model;

import com.google.gson.annotations.SerializedName;

public class AddReviewModel {

    @SerializedName("Customerid")
    private String customerId;

    @SerializedName("Userid")
    private String userId;

    @SerializedName("Title")
    private String title;

    @SerializedName("Star")
    private int star;

    @SerializedName("Discription")
    private String description;

    @SerializedName("Type")
    private String type;

    public AddReviewModel(){}

    public AddReviewModel(String customerId, String userId, String title, int star, String description, String type) {
        this.customerId = customerId;
        this.userId = userId;
        this.title = title;
        this.star = star;
        this.description = description;
        this.type = type;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AddReviewModel{" +
                "customerId='" + customerId + '\'' +
                ", userId='" + userId + '\'' +
                ", title='" + title + '\'' +
                ", star=" + star +
                ", description=" + description +
                ", type=" + type +
                '}';
    }
}
