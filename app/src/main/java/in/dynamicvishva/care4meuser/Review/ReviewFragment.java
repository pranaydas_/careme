package in.dynamicvishva.care4meuser.Review;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import in.dynamicvishva.care4meuser.Login.LoginActivity;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Review.Adapter.ReviewAdapter;
import in.dynamicvishva.care4meuser.Review.Model.ReviewModel;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import in.dynamicvishva.care4meuser.databinding.FragmentReviewBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewFragment extends Fragment {

    private Context mCon;
    private FragmentReviewBinding binding;
    private String userId, mobileNo;
    private ReviewAdapter reviewAdapter;
    private ProgressDialog progressDialog;
    private List<ReviewModel> reviewModelList = new ArrayList<>();

    public ReviewFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_review, container, false);
        mCon = getActivity();
        View view = binding.getRoot();
        progressDialog = new ProgressDialog(mCon);

        Window window = getActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));

        if (PreferenceUtil.getUser() != null) {
            userId = PreferenceUtil.getUser().getUserId();
            mobileNo = PreferenceUtil.getUser().getMobileNumber();
            binding.toolbar.setSubtitle(PreferenceUtil.getUser().getUserName());
        } else {
            new Intent(mCon, LoginActivity.class);
        }

        binding.logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceUtil.clearAll();
                startActivity(new Intent(mCon, LoginActivity.class));
                getActivity().finish();
            }
        });

        reviewAdapter = new ReviewAdapter(mCon);
        binding.reviewsRecycler.setLayoutManager(new LinearLayoutManager(mCon));
        binding.reviewsRecycler.setHasFixedSize(true);
        binding.reviewsRecycler.setItemAnimator(new DefaultItemAnimator());

        fetchReviews(userId);

        return view;
    }

    private void fetchReviews(String userId) {
        progressDialog.setMessage("Fetching Reviews");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<ReviewModel>> call = ApiClient.getNetworkService().fetchReviews(userId);
        call.enqueue(new Callback<List<ReviewModel>>() {
            @Override
            public void onResponse(Call<List<ReviewModel>> call, Response<List<ReviewModel>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.code() == 200)
                        {
                            reviewModelList = response.body();
                            Log.d("response", ""+reviewModelList);
                            reviewAdapter.addList(reviewModelList);
                            binding.reviewsRecycler.setAdapter(reviewAdapter);
                            progressDialog.dismiss();
                        }
                    }
                } else if (response.code() == 404) {
                    Toast.makeText(mCon, "Reviews Not Found", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    Log.d("check", "onResponse: " + mCon.getResources().getString(R.string.something_went_wrong));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ReviewModel>> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    Log.d("check", "" + t.getMessage());
                    progressDialog.dismiss();
                }
            }
        });
    }
}