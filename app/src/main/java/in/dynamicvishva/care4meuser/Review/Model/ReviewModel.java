package in.dynamicvishva.care4meuser.Review.Model;

import com.google.gson.annotations.SerializedName;

public class ReviewModel {

    @SerializedName("srId")
    private String srId;

    @SerializedName("customerId")
    private String customerId;

    @SerializedName("userId")
    private String userId;

    @SerializedName("title")
    private String title;

    @SerializedName("star")
    private int star;

    @SerializedName("discription")
    private String discription;

    @SerializedName("status")
    private String status;

    @SerializedName("createdBy")
    private Object createdBy;

    @SerializedName("Date")
    private String date;

    public ReviewModel() { }

    public ReviewModel(String srId, String customerId, String userId, String title, int star, String discription, String status, Object createdBy, String date) {
        this.srId = srId;
        this.customerId = customerId;
        this.userId = userId;
        this.title = title;
        this.star = star;
        this.discription = discription;
        this.status = status;
        this.createdBy = createdBy;
        this.date = date;
    }

    public String getSrId() {
        return srId;
    }

    public void setSrId(String srId) {
        this.srId = srId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ReviewModel{" +
                "srId='" + srId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", userId='" + userId + '\'' +
                ", title='" + title + '\'' +
                ", star='" + star + '\'' +
                ", discription='" + discription + '\'' +
                ", status='" + status + '\'' +
                ", createdBy=" + createdBy +
                ", date='" + date + '\'' +
                '}';
    }
}
