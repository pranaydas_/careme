package in.dynamicvishva.care4meuser.Login;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.concurrent.TimeUnit;

import in.dynamicvishva.care4meuser.MainActivity;
import in.dynamicvishva.care4meuser.Network.ApiClient;
import in.dynamicvishva.care4meuser.R;
import in.dynamicvishva.care4meuser.Signup.SignUpActivity;
import in.dynamicvishva.care4meuser.Util.PreferenceUtil;
import in.dynamicvishva.care4meuser.Util.ResponseCodes;
import in.dynamicvishva.care4meuser.databinding.ActivityLoginBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;

    private Context mCon;
    private String mobileNo, passwordStr;
    private TextInputLayout forgotMobileInputLayout,
            otpInputLayout, passwordInputLayout, conPasswordInputLayout;
    private TextInputEditText forgotMobileEditText,
            otpEditText, passwordEditText, conPasswordEditText;
    private BottomSheetDialog forgotPasswordDialog;
    private AppCompatImageView closeImageView;
    private String passStr, forgetMobileStr, otpStr, conPassStr;
    private MaterialButton submitButton, otpSubmitButton, passwordSubmitButton, resendOtpTextView;
    private LinearLayout mobileLinear, otpLiner, newPassLinear;
    private TextView timerTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        mCon = this;

        forgotPasswordDialog = new BottomSheetDialog(mCon);

        binding.forgotPassword.setOnClickListener(view -> forgotPasswordDialog.show());
        forgetPin();

        binding.callAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = "+918591079725";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });

        binding.mailAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mail = "arghyamcare@gmail.com";
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mail, null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Account approval for ArghyamCare - Customer App");
                startActivity(Intent.createChooser(intent, "Send email..."));
            }
        });

        binding.btnSignUp.setOnClickListener(v -> {
            startActivity(new Intent(mCon, SignUpActivity.class));
        });

        binding.btnLogIn.setOnClickListener(v -> {
            mobileNo = binding.mobileNoEditText.getText().toString().trim();
            passwordStr = binding.passwordEditText.getText().toString().trim();

            validate();
        });
    }

    private void validate() {
        boolean isValidMobileNo = false, isValidPass = false;

        if (TextUtils.isEmpty(mobileNo)) {
            binding.mobileNoInputLayout.setError("" + getResources().getString(R.string.field_cannot_be_empty));
        } else if (mobileNo.length() < 10) {
            binding.mobileNoInputLayout.setError("" + getResources().getString(R.string.enter_valid_no));
        } else if (!mobileNo.startsWith("6") && !mobileNo.startsWith("7") && !mobileNo.startsWith("8") && !mobileNo.startsWith("9")) {
            binding.mobileNoInputLayout.setError("" + getResources().getString(R.string.enter_valid_no));
        } else {
            binding.mobileNoInputLayout.setError(null);
            isValidMobileNo = true;
        }

        if (TextUtils.isEmpty(passwordStr)) {
            binding.passwordInputLayout.setError("" + getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.passwordInputLayout.setError(null);
            isValidPass = true;
        }

        if (isValidMobileNo && isValidPass) {
            login(mobileNo, passwordStr);
        }
    }

    private void login(String mobileNo, String password) {
        try {
            Call<String> call = ApiClient.getNetworkService().loginUser(mobileNo, password);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    handleResponse(response);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "login: LoginActivity " + e.getMessage());
        }
    }

    private void handleResponse(Response<String> response) {
        switch (response.code()) {

            case 404:

            case 403:
                binding.mobileNoEditText.setFocusable(true);
                binding.mobileNoInputLayout.setError("" + getResources().getString(R.string.mobile_not_registered));
                break;

            case 401:
                binding.passwordEditText.setFocusable(true);
                binding.passwordInputLayout.setError("" + getResources().getString(R.string.wrong_password));
                break;

            case 405:
                Toast.makeText(mCon, getResources().getString(R.string.not_approved), Toast.LENGTH_SHORT).show();
                break;

            case 200:
                String identify = response.body();

                if(identify != null)
                {
                    if (identify.equals("User")) {
                        PreferenceUtil.setUserLoggedIn(true);
                        Toast.makeText(mCon, R.string.login_sucess, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mCon, MainActivity.class);
                        intent.putExtra("mobileNo",mobileNo);
                        startActivity(intent);
                    }
                    else if(identify.equals("Customer")){
                        Toast.makeText(mCon, R.string.invalid_application, Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void forgetPin() {
        final View sheetView = getLayoutInflater().inflate(R.layout.forget_password_dialog, null);
        forgotPasswordDialog.setContentView(sheetView);
        forgotPasswordDialog.setCancelable(false);

        closeImageView = sheetView.findViewById(R.id.closeImageView);

        mobileLinear = sheetView.findViewById(R.id.mobileLinear);
        forgotMobileInputLayout = sheetView.findViewById(R.id.forgotMobileInputLayout);
        forgotMobileEditText = sheetView.findViewById(R.id.forgotMobileEditText);
        submitButton = sheetView.findViewById(R.id.submitButton);

        otpLiner = sheetView.findViewById(R.id.otpLiner);
        otpInputLayout = sheetView.findViewById(R.id.otpInputLayout);
        otpEditText = sheetView.findViewById(R.id.otpEditText);
        timerTextView = sheetView.findViewById(R.id.timerTextView);
        resendOtpTextView = sheetView.findViewById(R.id.resendOtpTextView);
        otpSubmitButton = sheetView.findViewById(R.id.otpSubmitButton);

        newPassLinear = sheetView.findViewById(R.id.newPassLinear);
        passwordInputLayout = sheetView.findViewById(R.id.passwordInputLayout);
        passwordEditText = sheetView.findViewById(R.id.passwordEditText);
        conPasswordInputLayout = sheetView.findViewById(R.id.conPasswordInputLayout);
        conPasswordEditText = sheetView.findViewById(R.id.conPasswordEditText);
        passwordSubmitButton = sheetView.findViewById(R.id.passwordSubmitButton);

        closeImageView.setOnClickListener(v -> {
            mobileLinear.setVisibility(View.VISIBLE);
            otpLiner.setVisibility(View.GONE);
            newPassLinear.setVisibility(View.GONE);
            forgotPasswordDialog.dismiss();
            forgotMobileEditText.setText("");
        });

        submitButton.setOnClickListener(v -> {
            forgetMobileStr = forgotMobileEditText.getText().toString().trim();
            validateForgetMobile();
        });

        otpSubmitButton.setOnClickListener(v -> {
            otpStr = otpEditText.getText().toString().trim();
            validateOTP();
        });

        resendOtpTextView.setOnClickListener(v -> {
            otpStr = otpEditText.getText().toString().trim();
            validateForgetMobile();
        });

        passwordSubmitButton.setOnClickListener(v -> {
            passStr = passwordEditText.getText().toString().trim();
            conPassStr = conPasswordEditText.getText().toString().trim();

            validateNewPass();
        });
    }

    private void validateForgetMobile() {
        boolean isValidForgetMob = false;

        if (TextUtils.isEmpty(forgetMobileStr)) {
            forgotMobileInputLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else if (forgetMobileStr.length() < 10) {
            forgotMobileInputLayout.setError(getResources().getString(R.string.enter_valid_no));
        } else if (!forgetMobileStr.startsWith("6") && !forgetMobileStr.startsWith("7") && !forgetMobileStr.startsWith("8") && !forgetMobileStr.startsWith("9")) {
            forgotMobileInputLayout.setError("" + getResources().getString(R.string.enter_valid_no));
        } else {
            forgotMobileInputLayout.setError(null);
            isValidForgetMob = true;
        }


        if (isValidForgetMob) {
            try {
                Call<String> call = ApiClient.getNetworkService().getOtp(forgetMobileStr);

                final ProgressDialog pd = new ProgressDialog(mCon);
                pd.setTitle(getResources().getString(R.string.app_name));
                pd.setMessage(getResources().getString(R.string.loading));
                pd.show();

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            // Toast.makeText(mCon, "" + response.body(), Toast.LENGTH_SHORT).show();
                            mobileLinear.setVisibility(View.GONE);
                            otpLiner.setVisibility(View.VISIBLE);
                            forgotMobileInputLayout.setError(null);
                            timerTextView.setVisibility(View.VISIBLE);
                            resendOtpTextView.setVisibility(View.GONE);
                            forgotMobileEditText.setText("");
                            startTimer();
                        } else if (response.code() == ResponseCodes.UNAUTHORIZED) {
                            forgotMobileInputLayout.setError(getResources().getString(R.string.enter_valid_no));
                            Toast.makeText(mCon, R.string.enter_valid_no, Toast.LENGTH_SHORT).show();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(forgotMobileEditText, InputMethodManager.SHOW_IMPLICIT);
                        } else if (response.code() == ResponseCodes.NOT_FOUND) {
                            Toast.makeText(mCon, "Mobile Number Does not Exist", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mCon, "Please try again", Toast.LENGTH_SHORT).show();
                        }

                        pd.dismiss();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                    }
                });
            } catch (Exception e) {
                Log.d("log", e.getMessage());
            }
        }
    }

    private void validateNewPass() {
        boolean isValidPassword = false, isValidConPass = false;

        if (TextUtils.isEmpty(passStr)) {
            passwordInputLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else if (passStr.length() < 4) {
            passwordInputLayout.setError(getResources().getString(R.string.passLength));
        } else {
            passwordInputLayout.setError(null);
            isValidPassword = true;
        }

        if (TextUtils.isEmpty(conPassStr)) {
            conPasswordInputLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else if (conPassStr.length() < 4) {
            conPasswordInputLayout.setError(getResources().getString(R.string.passLength));
        } else if (!conPassStr.equals(passStr)) {
            conPasswordInputLayout.setError(getResources().getString(R.string.passwords_do_not_match));
        } else {
            conPasswordInputLayout.setError(null);
            isValidConPass = true;
        }

        if (isValidPassword && isValidConPass) {
            try {
                Call<String> call = ApiClient.getNetworkService().resetPassword(forgetMobileStr, passStr);

                final ProgressDialog pd = new ProgressDialog(mCon);
                pd.setTitle(getResources().getString(R.string.app_name));
                pd.setMessage(getResources().getString(R.string.loading));
                pd.show();

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {

                            Toast.makeText(mCon, "" + response.body(), Toast.LENGTH_SHORT).show();
                            forgotMobileEditText.setText(null);
                            otpEditText.setText(null);
                            passwordEditText.setText(null);
                            conPasswordEditText.setText(null);
                            forgotMobileInputLayout.setError(null);
                            otpInputLayout.setError(null);
                            passwordInputLayout.setError(null);
                            conPasswordInputLayout.setError(null);
                            mobileLinear.setVisibility(View.VISIBLE);
                            otpLiner.setVisibility(View.GONE);
                            newPassLinear.setVisibility(View.GONE);
                            forgotPasswordDialog.dismiss();
                            resendOtpTextView.setVisibility(View.GONE);

                            startTimer();
                        } else {
                            Toast.makeText(mCon, "Please try again", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                    }
                });
            } catch (Exception e) {
                Log.d("log", e.getMessage());
            }
        }
    }

    private void validateOTP() {
        boolean isValidOtp = false;

        if (TextUtils.isEmpty(otpStr)) {
            otpInputLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            otpInputLayout.setError(null);
            isValidOtp = true;
        }

        if (isValidOtp) {
            try {
                Call<String> call = ApiClient.getNetworkService().sendOtp(forgetMobileStr, otpStr);

                final ProgressDialog pd = new ProgressDialog(mCon);
                pd.setTitle(getResources().getString(R.string.app_name));
                pd.setMessage(getResources().getString(R.string.loading));
                pd.show();

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            //Toast.makeText(mCon, "" + response.body(), Toast.LENGTH_SHORT).show();
                            otpLiner.setVisibility(View.GONE);
                            newPassLinear.setVisibility(View.VISIBLE);
                            otpInputLayout.setError(null);
                            startTimer();
                        } else if (response.code() == ResponseCodes.UNAUTHORIZED) {
                            otpInputLayout.setError(getResources().getString(R.string.invalid_otp));
                            otpEditText.requestFocus();
                            Toast.makeText(mCon, R.string.invalid_otp, Toast.LENGTH_SHORT).show();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(otpEditText, InputMethodManager.SHOW_IMPLICIT);
                        } else if (response.code() == ResponseCodes.NOT_FOUND) {
                            Toast.makeText(mCon, "OTP did not match", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mCon, "Please try again", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                    }
                });
            } catch (Exception e) {
                Log.d("log", e.getMessage());
            }
        }
    }

    private void startTimer() {
        new CountDownTimer(120000, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                @SuppressLint("DefaultLocale") String tick = "" + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

                timerTextView.setText(tick);
            }

            public void onFinish() {
                timerTextView.setVisibility(View.GONE);
                resendOtpTextView.setVisibility(View.VISIBLE);
            }
        }.start();
    }
}